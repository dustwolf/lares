<?php

require_once "frames.php";

/*


*/

class motor {

 private $naslov;
 private $id;
 public $common;
 public $stanje;
 public $fokus;
 private $doc;
 private $fail;

 function __construct($naslov, $id = "", $stanje = "", $fokus = False, $doc = "") {
  $this->naslov = $naslov;

  if($stanje == "") {
   $stanje = (object) True;
  }
 
  if($id == "") {

   require_once "mysqli.php";
   $db = new dblink();

   $podatki = $db->q("
    SELECT `motor`, `stanje` FROM `okvircki`
     WHERE `naslov` = '".$db->e($this->naslov)."'
     LIMIT 1
   ");

   if(isset($podatki[0])) {
    $this->id = $podatki[0]["motor"];
    $this->stanje = json_decode($podatki[0]["stanje"]);
    $this->fail = False;
   } else {
    $this->fail = True;
   }

  } else {

   $this->id = $id;
   $this->stanje = $stanje;
   $this->fail = False;

  }

  $this->fokus = $fokus;

  $this->doc = $doc;

  if(!$this->fail) {
   $common = (object) True;
   include "engines/".$this->id."/common.php";
   $this->common = $common;
  }
 }

 function gui($overrideHtml = "") {

  if($this->fokus) {
   $this->megaGui($overrideHtml);
  } else {
   $this->tinyGui($overrideHtml);
  }

 }

 function tinyGui($overrideHtml = "", $ajax = False) {

  $stanje = $this->stanje;
  $velikost = "tiny";

  if($overrideHtml == "") {
   ob_start();
    include "engines/".$this->id."/gui.php";
   $html = ob_get_clean();
  } else {
   $html = $overrideHtml;
  }

  //ALI JE TO AJAX REFRESH REQUEST ALI NE
  if(!$ajax) {

   //ALI JE MODUL VKLOPLJEN
   if(isset($this->stanje->vklop) && $this->stanje->vklop == "1") {
    $on = 1;
   } else {
    $on = 0;
   }

   new okvircek($this->common->tip, $this->common->ikona, $on, $this->naslov, $html, ($overrideHtml == ""));

  } else {

   echo $html;

  }

 }

 function megaGui($overrideHtml = "", $ajax = False) {

  //NALOŽI OBSTOJEČE NASTAVITVE v assoc array
  $nastavitve = json_decode(json_encode($this->stanje), True);
  $stanje = $this->stanje;
  $velikost = "mega";

  if($overrideHtml == "") {
   ob_start();
    include "engines/".$this->id."/gui.php";

    //PRIKAŽI VMESNIK ZA SPREMINJANJE RUNTIME NASTAVITEV
     ?>
      <br><br>
      <form method="post">
       <input type="hidden" name="id" value="<?php echo $this->naslov; ?>">
      <?php
      if(isset($this->common->runtime)) {
       foreach($this->common->runtime as $nastavitev) {
        ?>
         <label><?php echo $nastavitev; ?></label>
         <input class="form-control" type="text" 
                name="<?php echo $nastavitev; ?>" value="<?php echo $nastavitve[$nastavitev]; ?>"><br>
        <?php    
       }
      }
      ?>
       <input class="form-control btn btn-primary urejaj-inline-block" type="submit" value="Shrani">
      </form>
     <?php

   $html = ob_get_clean();
  } else {
   $html = $overrideHtml;
  }

  if(file_exists('engines/'.$this->id.'/gui-static.php')) {
   ob_start();
    include "engines/".$this->id."/gui-static.php";
    ?><br><br><?php
   $staticna = ob_get_clean();
  } else {
   $staticna = "";
  }
 

  //ALI JE TO AJAX REFRESH REQUEST ALI NE
  if(!$ajax) {

   //ALI JE MODUL VKLOPLJEN
   if(isset($this->stanje->vklop) && $this->stanje->vklop == "1") {
    $on = 1;
   } else {
    $on = 0;
   }

   //ALI NAJ SE MODUL OSVEŽUJE Z AJAXOM
   $ajaxRefresh = ($overrideHtml == "");
   if(isset($this->common->ajaxRefresh)) {
    $ajaxRefresh = $this->common->ajaxRefresh;
   }
   new okvir($this->common->tip, $this->common->ikona, $on, $this->naslov, $html, $ajaxRefresh, $staticna);
 
  } else {

   echo $html;

  }

 }

 function control() {
  if(!$this->fail) {


  include "engines/".$this->id."/control.php";

  //SHRANI NOVO STANJE V BAZO
  require_once "mysqli.php";
  $db = new dblink();

  $db->q("
   UPDATE `okvircki`
    SET `stanje` = '".$db->e(json_encode($this->stanje))."'
    WHERE `naslov` = '".$db->e($this->naslov)."'
  ");

  }
 }

 function nastavi($shrani = array()) {
  if(!$this->fail) {

  /*

   TODO:
				* Ob vsaki nastavitvi, je potrebno zabeležiti kateri modul je to bil
    * Ob koncu control zanke, se morajo ponovno izvesti vsi controli teh modulov

  */

  //NALOŽI OBSTOJEČE NASTAVITVE v assoc array
  $nastavitve = json_decode(json_encode($this->stanje), True);

  //DOPOLNI MANJKAJOČA INTERNA STANJA
  if(isset($this->common->runtime)) {
   foreach ($this->common->runtime as $var) {
    if(!isset($nastavitve[$var])) {
     $nastavitve[$var] = 0;
    }
   }
  }
 
  if(isset($this->common->podatki)) {
   foreach ($this->common->podatki as $var) {
    if(!isset($nastavitve[$var])) {
     $nastavitve[$var] = 0;
    }
   }
  }

  //DOPOLNI PODANE NASTAVITVE
  if($shrani != array()) {
   foreach ($shrani as $lastnost => $vrednost) {
    $nastavitve[$lastnost] = $vrednost;
    $this->stanje->{$lastnost} = $vrednost;
   }
 
   //SHRANI NASTAVITVE V BAZO
   require_once "mysqli.php";
   $db = new dblink();
 
   $db->q("
    UPDATE `okvircki`
     SET `stanje` = '".$db->e(json_encode($nastavitve))."'
     WHERE `naslov` = '".$db->e($this->naslov)."'
   ");


   if(isset($nastavitve["naslov"]) && isset($shrani["id"]) && $nastavitve["naslov"] != $shrani["id"]) {
    $db->q("
     UPDATE `okvircki`
      SET `naslov` = '".$db->e($nastavitve["naslov"])."'
      WHERE `naslov` = '".$db->e($nastavitve["id"])."'
    ");

   $this->naslov = $nastavitve["naslov"];
   }

   //ZAPIŠI LOG ZA CONTROL
   $db->q("
    INSERT IGNORE INTO `preveri` (`naslov`)
     VALUES ('".$db->e($this->naslov)."')
   ");

  }

  }
 }

 function nastavitve() {

  //NALOŽI OBSTOJEČE NASTAVITVE v assoc array
  $nastavitve = json_decode(json_encode($this->stanje), True);

  //PRIKAŽI VMESNIK ZA SPREMINJANJE NASTAVITEV
  ob_start();

   ?>
    <form method="post">
     <input type="hidden" name="id" value="<?php echo $this->naslov; ?>">
     <label>naslov</label>
     <input class="form-control" type="text" name="naslov" value="<?php echo $this->naslov; ?>"><br>
    <?php
    if(isset($this->common->nastavitve)) {
     foreach($this->common->nastavitve as $nastavitev) {
      ?>
       <label><?php echo $nastavitev; ?></label>
       <input class="form-control" type="text" 
              name="<?php echo $nastavitev; ?>" value="<?php echo $nastavitve[$nastavitev]; ?>"><br>
      <?php    
     }
    }

    if(file_exists("engines/".$this->id."/setup.php")) {
     include "engines/".$this->id."/setup.php";
    }

   ?>
    <input class="form-control btn btn-primary" type="submit" value="Shrani">
   </form>
  <?php

  $html = ob_get_clean();

  new okvir($this->common->tip, $this->common->ikona, $nastavitve['vklop'], "", $html, False);
  
 }

 function elementTipka($napis = "", $parameter = "vklop", $vrednost = "0", $barva = "default") {
  ?><button class="btn btn-<?php echo $barva; ?>" onclick="$.ajax({url: 'ajaxSettings.php?id=<?php echo rawurlencode($this->naslov); ?>&<?php echo rawurlencode($parameter) ?>=<?php echo rawurlencode($vrednost) ?>'})"><?php echo $napis; ?></button> <?php
 }

}

class motorji {

 public $seznam;
 
 function __construct() {

  require_once "mysqli.php";
  $db = new dblink();

  $this->seznam = $db->flatten($db->q("
   SELECT `naslov` FROM `okvircki`
  ","naslov"), "naslov");
  
 }

 function kiImajo($kriterij = "runtime") {

  foreach ($this->seznam as $i => $modul) {
   $motor = new motor($modul);
   if(!isset($motor->common->{$kriterij}) || count($motor->common->{$kriterij}) == 0) {
    unset($this->seznam[$i]);
   }
  }

 }

 function kiImaNastavitev($lastnost, $vrednost) {

  foreach ($this->seznam as $i => $modul) {
   $motor = new motor($modul);
   if(!isset($motor->stanje->{$lastnost}) 
          || $motor->stanje->{$lastnost} != $vrednost) {
    unset($this->seznam[$i]);
   }
  }

 }

}

require_once "html.php";

class modulPicker {

 private $doc;
 private $motorji;
 private $kaksni;
 private $stanje;

 private $modul;

 function __construct($doc, $stanje, $kaksni) {
 
  $this->doc = $doc;
  $this->stanje = $stanje;
  $this->kaksni = $kaksni;
 
  $this->motorji = new motorji();
  $this->motorji->kiImajo($this->kaksni);

  $this->modul = "";

 }

 function moduli($naslov = "modul", $parameter = "modul") {
  $drop = new dropdown($this->doc, $naslov, $parameter, $this->stanje);
  $drop->opcija();
  foreach ($this->motorji->seznam as $modul) {
   $drop->opcija($modul);
  }
  $this->modul = $parameter;
 }

 function parametri($naslov = "parameter", $parameter = "parameter") {
  $drop = new dropdown($this->doc, $naslov, $parameter, $this->stanje, $this->modul, $this->kaksni);
  $drop->opcija($this->stanje->{$parameter});
 }

}

class odstraniMotor {

 private $naslov;
 private $id;
 private $common;

 function __construct($naslov, $id = "") {
  $this->naslov = $naslov;

  if($id == "") {

   //pridobi engine iz baze
   require_once "mysqli.php";
   $db = new dblink();

   $okvircki = $db->q("
    SELECT `motor` 
     FROM `okvircki`
     WHERE `naslov` = '".$db->e($naslov)."'
     LIMIT 1
   ");

   $this->id = $okvircki[0]["motor"];
   
  } else {
   $this->id = $id;   
  }

  $common = (object) True;
  include "engines/".$this->id."/common.php";
  $this->common = $common;
 }

 function uninstall() {

  require_once "mysqli.php";
  $db = new dblink();

  $db->q("
   DELETE FROM `okvircki`
   WHERE `naslov` = '".$db->e($this->naslov)."'
  ");
  
 }

}

class dodajMotor {

 private $naslov;
 private $id;
 public $common;
 private $html;

 function __construct($naslov, $id, $html = "") {
  $this->naslov = $naslov;
  $this->id = $id;
  if($html == "") {
   $html = "Add new ".$id;
  } 
  $this->html = $html;

  $common = (object) True;
  include "engines/".$this->id."/common.php";
  $this->common = $common;
 }

 function gui() {

  new okvircek($this->common->tip, $this->common->ikona, -1, $this->naslov, $this->html, False);

 }

 function install() {

  require_once "mysqli.php";
  $db = new dblink();

  $stanje = array();
  $stanje["vklop"] = 0;
  if(isset($this->common->nastavitve)) { 
   foreach($this->common->nastavitve as $nastavitev) {
    $stanje[$nastavitev] = "";
   }
  }
  if(isset($this->common->podatki)) { 
   foreach($this->common->podatki as $nastavitev) {
    $stanje[$nastavitev] = 0;
   }
  }  
  if(isset($this->common->runtime)) { 
   foreach($this->common->runtime as $nastavitev) {
    $stanje[$nastavitev] = 0;
   }
  }  

  $db->q("
   INSERT IGNORE INTO `okvircki` (`naslov`, `motor`, `stanje`)
   VALUES ('".$db->e($this->naslov)."',
           '".$db->e($this->id)."',
           '".$db->e(json_encode($stanje))."')
  ");
  
 }

}

?>
