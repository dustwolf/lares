<?php

/*


*/

class okvir {

 function __construct($tip = "aktuator", $ikona = "motor", $aktivnost = 0, $naslov = "Neimenovan", $vsebina = "/", $ajax = True, $staticna = "") {
  
  include "colors.php";

?>
 <div class="wrapper">
  <div class="icon">
   <div class="iconColor" style="background-color: <?php echo $barva; ?>; " id="<?php echo md5($naslov); ?>-iconColor">
    <div class="iconCore" style="background-image: url('icons/<?php echo $ikona; ?>.png'); ">
     <img id="<?php echo md5($naslov); ?>-iconSpin" class="iconSpin" src="icons/<?php echo $tip; ?>.png" alt="<?php echo $tip; ?>">
    </div>
   </div>
   <div class="corner"></div>
  </div>
  <div class="content">
   <h1><?php echo $naslov; ?></h1>
   <?php echo $staticna; ?>
   <?php if($ajax) { ?><button id="editButton" style="float: right;" class="btn btn-warning" onclick="window.urejaj = true">Urejaj</button><?php } ?>
   <div id="<?php echo md5($naslov); ?>-content"><?php echo $vsebina; ?></div>
  </div>
  <?php if($ajax) { ?>
   <script>
    setInterval(function(){
     if(window.urejaj) {
      jQuery("#<?php echo md5($naslov); ?>-content").css("border", "red solid 3px");
      jQuery("#<?php echo md5($naslov); ?>-content").css("padding", "10px");
      jQuery("#editButton").css("display", "none");       
      jQuery(".urejaj-inline-block").css("display", "inline-block");       
      jQuery(".urejaj-table-cell").css("display", "table-cell");       
      jQuery(".urejaj-table-row").css("display", "table-row");       
     } else {     
      jQuery.ajax("refresh.php?update=<?php echo rawurlencode($naslov); ?>&velikost=mega").done(
       function(json) { data = jQuery.parseJSON(json); 
        jQuery("#<?php echo md5($naslov); ?>-content").html(data.html);
        jQuery("#<?php echo md5($naslov); ?>-iconColor").css("background-color", data.iconColor);
        window.Active<?php echo md5($naslov); ?> = data.active;
       }
      );
     }
    }, 1000);
    window.window.angle<?php echo md5($naslov); ?> = 0;
    setInterval(function(){
     if(window.Active<?php echo md5($naslov); ?>) {
      jQuery("#<?php echo md5($naslov); ?>-iconSpin").css("transition","all 0s");
      window.angle<?php echo md5($naslov); ?> += 3;
     } else {
      jQuery("#<?php echo md5($naslov); ?>-iconSpin").css("transition","all 1s linear");
      window.angle<?php echo md5($naslov); ?> = 0;
     }
     jQuery("#<?php echo md5($naslov); ?>-iconSpin").rotate(window.angle<?php echo md5($naslov); ?>);
    },50);
   </script>
  <?php } ?>
 </div>
<?php
 }

}

class okvircek {

 function __construct($tip = "aktuator", $ikona = "motor", $aktivnost = 0, $naslov = "Neimenovan", $vsebina = "/", $ajax = True) {

  include "colors.php";

?>
<a href="?fokus=<?php echo rawurlencode($naslov); ?>">
 <div class="wrapper">
  <div class="icon iconSmall">
   <div class="iconColor iconColorSmall" style="background-color: <?php echo $barva; ?>; " id="<?php echo md5($naslov); ?>-iconColor">
    <div class="iconCore iconCoreSmall" style="background-image: url('icons/<?php echo $ikona; ?>.png'); ">
     <img id="<?php echo md5($naslov); ?>-iconSpin" class="iconSpin" src="icons/<?php echo $tip; ?>.png" alt="<?php echo $tip; ?>">
    </div>
   </div>
  </div>
  <div class="content contentSmall">
   <b><?php echo $naslov; ?></b><div id="<?php echo md5($naslov); ?>-content"><?php echo $vsebina; ?></div>
  </div>
  <?php if($ajax) { ?>
   <script>
    setInterval(function(){
     jQuery.ajax("refresh.php?update=<?php echo rawurlencode($naslov); ?>&velikost=tiny").done(
      function(json) { data = jQuery.parseJSON(json); 
       jQuery("#<?php echo md5($naslov); ?>-content").html(data.html); 
       jQuery("#<?php echo md5($naslov); ?>-iconColor").css("background-color", data.iconColor);
       window.Active<?php echo md5($naslov); ?> = data.active;
      }
     );
    }, 1000);
    window.window.angle<?php echo md5($naslov); ?> = 0;
    setInterval(function(){
     if(window.Active<?php echo md5($naslov); ?>) {
      jQuery("#<?php echo md5($naslov); ?>-iconSpin").css("transition","all 0s");
      window.angle<?php echo md5($naslov); ?> += 3;
     } else {
      jQuery("#<?php echo md5($naslov); ?>-iconSpin").css("transition","all 1s linear");
      window.angle<?php echo md5($naslov); ?> = 0;
     }
     jQuery("#<?php echo md5($naslov); ?>-iconSpin").rotate(window.angle<?php echo md5($naslov); ?>);
    },50);
   </script>
  <?php } ?>
 </div>
</a>
<?php
 }

}

?>
