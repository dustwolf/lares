-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lares
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `okvircki`
--

LOCK TABLES `okvircki` WRITE;
/*!40000 ALTER TABLE `okvircki` DISABLE KEYS */;
INSERT INTO `okvircki` VALUES ('Komande','tipkovnica','{\"vklop\":1,\"Tipka-A\":\"Odpri ventile\",\"vrednostA\":\"1\",\"Tipka-B\":\"Zapri ventile\",\"vrednostB\":\"1\",\"Tipka-C\":\"\",\"vrednostC\":\"\",\"Tipka-D\":\"\",\"vrednostD\":\"\",\"stanje\":0,\"napaka\":0,\"modulA\":\"Odpri vse ventile\",\"parameterA\":\"vklop\",\"modulB\":\"Zapri vse ventile\",\"parameterB\":\"vklop\",\"modulC\":\"0\",\"parameterC\":\"0\",\"modulD\":\"0\",\"parameterD\":\"0\",\"id\":\"Komande\",\"naslov\":\"Komande\"}'),('Kontakt 1','regulator','{\"vklop\":\"1\",\"modulSens\":\"Senzor #1\",\"parameterSens\":\"vrednost\",\"vrednost\":\"0\",\"modulAct\":\"Odpri vse ventile\",\"parameterAct\":\"trigger\",\"spodnja-meja\":\"0\",\"zgornja-meja\":\"1\",\"signal-pod\":\"0\",\"signal-ok\":\"1\",\"signal-nad\":\"1\",\"id\":\"Kontakt\",\"naslov\":\"Kontakt 1\",\"signal\":\"0\"}'),('Kontakt 2','regulator','{\"vklop\":\"1\",\"modulSens\":\"Senzor #2\",\"parameterSens\":\"vrednost\",\"vrednost\":\"0\",\"modulAct\":\"Zapri vse ventile\",\"parameterAct\":\"trigger\",\"signal\":\"0\",\"spodnja-meja\":\"0\",\"zgornja-meja\":\"1\",\"signal-pod\":\"0\",\"signal-ok\":\"1\",\"signal-nad\":\"1\",\"id\":\"Kontakt 2\",\"naslov\":\"Kontakt 2\"}'),('Odpri vse ventile','programator','{\"vklop\":\"0\",\"timer-stanje\":-16,\"ex-trigger\":\"0\",\"timer\":0,\"korak\":1,\"trigger\":\"0\",\"timer-zadnjic\":1445191371,\"id\":\"Odpri vse ventile\",\"naslov\":\"Odpri vse ventile\"}'),('Senzor #1','digitalni senzor','{\"vklop\":\"1\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"port\":\"0\",\"stanje\":0,\"demo\":\"0\",\"id\":\"Senzor #1\",\"naslov\":\"Senzor #1\",\"vrednost\":\"0\"}'),('Senzor #2','digitalni senzor','{\"vklop\":\"1\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"port\":\"1\",\"vrednost\":\"0\",\"demo\":\"0\",\"id\":\"Senzor #2\",\"naslov\":\"Senzor #2\"}'),('Stanje','panel','{\"vklop\":1,\"Panel-A\":\"Ni kontakta; danger\",\"Panel-B\":\"Zaprt; danger\",\"Panel-C\":\"0\",\"Panel-D\":\"0\",\"id\":\"Stanje\",\"naslov\":\"Stanje\"}'),('Stanje senzorja','regulator','{\"vklop\":\"1\",\"modulSens\":\"Senzor #1\",\"parameterSens\":\"vrednost\",\"vrednost\":\"0\",\"modulAct\":\"Stanje\",\"parameterAct\":\"Panel-A\",\"signal\":\"Ni kontakta; danger\",\"spodnja-meja\":\"0\",\"zgornja-meja\":\"1\",\"signal-pod\":\"Ni kontakta; danger\",\"signal-ok\":\"Kontakt je; success\",\"signal-nad\":\"Kontakt je; success\",\"id\":\"Stanje senzorja\",\"naslov\":\"Stanje senzorja\"}'),('Stanje ventila','regulator','{\"vklop\":\"1\",\"modulSens\":\"Ventil 1 - vstopni\",\"parameterSens\":\"stanje\",\"vrednost\":\"0\",\"modulAct\":\"Stanje\",\"parameterAct\":\"Panel-B\",\"signal\":\"Zaprt; danger\",\"spodnja-meja\":\"0\",\"zgornja-meja\":\"1\",\"signal-pod\":\"Zaprt; danger\",\"signal-ok\":\"Odprt; success\",\"signal-nad\":\"Odprt; success\",\"id\":\"Stanje ventila\",\"naslov\":\"Stanje ventila\"}'),('Ventil 1 - vstopni','ventil','{\"vklop\":\"0\",\"port\":\"0\",\"id\":\"Ventil 1 - vstopni\",\"naslov\":\"Ventil 1 - vstopni\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Ventil 2 - RO waste','ventil','{\"vklop\":\"0\",\"port\":\"1\",\"id\":\"Ventil 2 - RO waste\",\"naslov\":\"Ventil 2 - RO waste\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Ventil 3 - IIS vstopni','ventil','{\"vklop\":\"0\",\"port\":\"2\",\"id\":\"Ventil 3 - IIS vstopni\",\"naslov\":\"Ventil 3 - IIS vstopni\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Ventil 4 - IIS waste','ventil','{\"vklop\":\"0\",\"port\":\"3\",\"id\":\"Ventil 4 - IIS waste\",\"naslov\":\"Ventil 4 - IIS waste\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Ventil 5 - končni','ventil','{\"vklop\":\"0\",\"port\":\"4\",\"id\":\"Ventil 5 - kon\\u010dni\",\"naslov\":\"Ventil 5 - kon\\u010dni\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Zapri vse ventile','programator','{\"vklop\":\"0\",\"timer-stanje\":-14,\"ex-trigger\":\"0\",\"timer\":0,\"korak\":1,\"trigger\":\"0\",\"timer-zadnjic\":1445191371,\"id\":\"Zapri vse ventile\",\"naslov\":\"Zapri vse ventile\"}');
/*!40000 ALTER TABLE `okvircki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `preveri`
--

LOCK TABLES `preveri` WRITE;
/*!40000 ALTER TABLE `preveri` DISABLE KEYS */;
INSERT INTO `preveri` VALUES ('Kontakt 1'),('Kontakt 2'),('Stanje senzorja'),('Stanje ventila');
/*!40000 ALTER TABLE `preveri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `programi`
--

LOCK TABLES `programi` WRITE;
/*!40000 ALTER TABLE `programi` DISABLE KEYS */;
INSERT INTO `programi` VALUES ('Izklop',1,'Senzor #1','vrednost','<','1'),('Izklop',2,'Ventil 1 - vstopni','vklop','=','0'),('Odpri vse ventile',1,'Ventil 1 - vstopni','vklop','=','1'),('Odpri vse ventile',2,'Ventil 2 - RO waste','vklop','=','1'),('Odpri vse ventile',3,'Ventil 3 - IIS vstopni','vklop','=','1'),('Odpri vse ventile',4,'Ventil 4 - IIS waste','vklop','=','1'),('Odpri vse ventile',5,'Ventil 5 - končni','vklop','=','1'),('Odpri vse ventile',6,'Odpri vse ventile','vklop','=','0'),('Vklop',1,'Senzor #1','vrednost','>','0'),('Vklop',2,'Ventil 1 - vstopni','vklop','=','1'),('Zapri vse ventile',1,'Ventil 1 - vstopni','vklop','=','0'),('Zapri vse ventile',2,'Ventil 2 - RO waste','vklop','=','0'),('Zapri vse ventile',3,'Ventil 3 - IIS vstopni','vklop','=','0'),('Zapri vse ventile',4,'Ventil 4 - IIS waste','vklop','=','0'),('Zapri vse ventile',5,'Ventil 5 - končni','vklop','=','0'),('Zapri vse ventile',6,'Zapri vse ventile','vklop','=','0');
/*!40000 ALTER TABLE `programi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-18 20:02:52
