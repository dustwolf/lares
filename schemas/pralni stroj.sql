-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lares
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `okvircki`
--

LOCK TABLES `okvircki` WRITE;
/*!40000 ALTER TABLE `okvircki` DISABLE KEYS */;
INSERT INTO `okvircki` VALUES ('Boben','motor','{\"vklop\":\"0\",\"rpm\":\"2000\",\"id\":\"Boben\",\"naslov\":\"Boben\"}'),('Boben naprej nazaj','programator','{\"vklop\":\"0\",\"ime\":\"Vrtenje 1\",\"id\":\"Boben naprej nazaj\",\"naslov\":\"Boben naprej nazaj\",\"timer\":0,\"korak\":\"0\",\"timer-zadnjic\":1431103978,\"timer-stanje\":0}'),('Črpalka','motor','{\"vklop\":\"0\",\"rpm\":\"5000\",\"id\":\"\\u010crpalka\",\"naslov\":\"\\u010crpalka\"}'),('Dovod sveže vode','ventil','{\"vklop\":\"0\",\"port\":\"3\",\"id\":\"Dovod sve\\u017ee vode\",\"naslov\":\"Dovod sve\\u017ee vode\"}'),('Dovod v pos. za prašek','ventil','{\"vklop\":\"0\",\"port\":\"1\",\"id\":\"Dovod v pos. za pra\\u0161ek\",\"naslov\":\"Dovod v pos. za pra\\u0161ek\"}'),('Gladina vode','senzor','{\"vklop\":\"1\",\"port\":\"2\",\"enota\":\"mm\",\"vrednost\":12.48,\"id\":\"Gladina vode\",\"naslov\":\"Gladina vode\",\"demo\":\"1\",\"demo-vrednost\":\"10\"}'),('Glavni','programator','{\"vklop\":\"0\",\"ime\":\"Pralni stroj 1\",\"naslov\":\"Glavni\",\"id\":\"Glavni\",\"nastavitev_timerja\":\"0\",\"timer\":-5,\"korak\":\"0\",\"0\":-4,\"timer-stanje\":-5,\"timer-zadnjic\":1431103978}'),('Graf gladine','graf','{\"vklop\":\"1\",\"os-Y\":\"mm\",\"crta-A\":\"Gladina vode\",\"crta-B\":\"\",\"crta-C\":\"\",\"crta-D\":\"\",\"trenutni\":\"grafi\\/Graf gladine.rrd\",\"zacetek\":0,\"modulA\":\"Gladina vode\",\"modulB\":\"0\",\"modulC\":\"0\",\"modulD\":\"0\",\"parameterA\":\"vrednost\",\"parameterB\":\"0\",\"parameterC\":\"0\",\"parameterD\":\"0\",\"barvaA\":\"#0000ff\",\"barvaB\":\"#000000\",\"barvaC\":\"#000000\",\"barvaD\":\"#000000\",\"vrednostA\":12.87,\"vrednostB\":\"U\",\"vrednostC\":\"U\",\"vrednostD\":\"U\",\"obdobje\":\"-1200\",\"id\":\"Graf gladine\",\"naslov\":\"Graf gladine\"}'),('Graf temperature','graf','{\"vklop\":\"1\",\"os-Y\":\"\\u00b0C\",\"crta-A\":\"Temperatura vode\",\"crta-B\":\"\",\"crta-C\":\"\",\"crta-D\":\"\",\"trenutni\":\"grafi\\/Graf temperature.rrd\",\"zacetek\":0,\"modulA\":\"Temperatura vode\",\"modulB\":\"0\",\"modulC\":\"0\",\"modulD\":\"0\",\"parameterA\":\"vrednost\",\"parameterB\":\"0\",\"parameterC\":\"0\",\"parameterD\":\"0\",\"barvaA\":\"#ff0000\",\"barvaB\":\"#000000\",\"barvaC\":\"#000000\",\"barvaD\":\"#000000\",\"vrednostA\":22.83,\"vrednostB\":\"U\",\"vrednostC\":\"U\",\"vrednostD\":\"U\",\"obdobje\":\"-1200\",\"id\":\"Graf temperature\",\"naslov\":\"Graf temperature\"}'),('Gretje vode','regulator','{\"vklop\":\"0\",\"nastavitev\":\"90\",\"id\":\"Gretje vode\",\"naslov\":\"Gretje vode\"}'),('Izpust za odplake','ventil','{\"vklop\":\"0\",\"port\":\"2\",\"id\":\"Izpust za odplake\",\"naslov\":\"Izpust za odplake\"}'),('Ponastavitev','programator','{\"vklop\":\"0\",\"ime\":\"Ponastavi 1\",\"timer-stanje\":-4139,\"timer\":-4139,\"korak\":12,\"timer-zadnjic\":1431103978,\"id\":\"Ponastavitev\",\"naslov\":\"Ponastavitev\"}'),('Simulacija','simulator','{\"vklop\":\"1\",\"id\":\"Simulacija\",\"naslov\":\"Simulacija\"}'),('Temperatura vode','senzor','{\"vklop\":\"1\",\"port\":\"1\",\"enota\":\"\\u00b0C\",\"vrednost\":23.81,\"id\":\"Temperatura vode\",\"naslov\":\"Temperatura vode\",\"demo\":\"1\",\"demo-vrednost\":\"22.5\"}');
/*!40000 ALTER TABLE `okvircki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `programi`
--

LOCK TABLES `programi` WRITE;
/*!40000 ALTER TABLE `programi` DISABLE KEYS */;
INSERT INTO `programi` VALUES ('Boben naprej nazaj',1,'Boben','rpm','=','200'),('Boben naprej nazaj',2,'Boben','vklop','=','1'),('Boben naprej nazaj',3,'Boben naprej nazaj','timer','=','3'),('Boben naprej nazaj',4,'Boben naprej nazaj','timer-stanje','<','1'),('Boben naprej nazaj',5,'Boben','vklop','=','0'),('Boben naprej nazaj',6,'Boben naprej nazaj','timer','=','3'),('Boben naprej nazaj',7,'Boben naprej nazaj','timer-stanje','<','1'),('Boben naprej nazaj',8,'Boben','rpm','=','-200'),('Boben naprej nazaj',9,'Boben','vklop','=','1'),('Boben naprej nazaj',10,'Boben naprej nazaj','timer','=','3'),('Boben naprej nazaj',11,'Boben naprej nazaj','timer-stanje','<','1'),('Boben naprej nazaj',12,'Boben','vklop','=','0'),('Boben naprej nazaj',13,'Boben naprej nazaj','timer','=','3'),('Boben naprej nazaj',14,'Boben naprej nazaj','timer-stanje','<','1'),('Boben naprej nazaj',15,'Boben naprej nazaj','korak','=','1'),('Glavni',1,'Dovod sveže vode','vklop','=','1'),('Glavni',3,'Gretje vode','vklop','=','1'),('Glavni',4,'Gladina vode','vrednost','>','500'),('Glavni',5,'Dovod sveže vode','vklop','=','0'),('Glavni',6,'Temperatura vode','vrednost','>','85'),('Glavni',7,'Gretje vode','vklop','=','0'),('Glavni',8,'Boben naprej nazaj','vklop','=','1'),('Glavni',9,'Dovod v pos. za prašek','vklop','=','1'),('Glavni',10,'Črpalka','vklop','=','1'),('Glavni',11,'Glavni','timer','=','10'),('Glavni',12,'Glavni','timer-stanje','<','1'),('Glavni',13,'Črpalka','vklop','=','0'),('Glavni',14,'Dovod v pos. za prašek','vklop','=','0'),('Glavni',15,'Glavni','timer','=','60'),('Glavni',16,'Glavni','timer-stanje','<','1'),('Glavni',17,'Izpust za odplake','vklop','=','1'),('Glavni',18,'Črpalka','vklop','=','1'),('Glavni',19,'Gladina vode','vrednost','<','40'),('Glavni',20,'Črpalka','vklop','=','0'),('Glavni',21,'Izpust za odplake','vklop','=','0'),('Glavni',22,'Dovod sveže vode','vklop','=','1'),('Glavni',23,'Gladina vode','vrednost','>','500'),('Glavni',24,'Dovod sveže vode','vklop','=','0'),('Glavni',25,'Glavni','timer','=','30'),('Glavni',26,'Glavni','timer-stanje','<','1'),('Glavni',27,'Izpust za odplake','vklop','=','1'),('Glavni',28,'Črpalka','vklop','=','1'),('Glavni',29,'Gladina vode','vrednost','<','40'),('Glavni',30,'Črpalka','vklop','=','0'),('Glavni',31,'Izpust za odplake','vklop','=','0'),('Glavni',32,'Boben naprej nazaj','vklop','=','0'),('Glavni',33,'Boben','rpm','=','2000'),('Glavni',34,'Izpust za odplake','vklop','=','1'),('Glavni',35,'Črpalka','vklop','=','1'),('Glavni',36,'Glavni','timer','=','30'),('Glavni',37,'Glavni','timer-stanje','<','1'),('Glavni',38,'Ponastavitev','vklop','=','1'),('Glavni',39,'Glavni','vklop','=','0'),('Ponastavitev',1,'Glavni','vklop','=','0'),('Ponastavitev',2,'Glavni','korak','=','0'),('Ponastavitev',3,'Boben naprej nazaj','vklop','=','0'),('Ponastavitev',4,'Boben naprej nazaj','korak','=','0'),('Ponastavitev',5,'Boben','vklop','=','0'),('Ponastavitev',6,'Črpalka','vklop','=','0'),('Ponastavitev',7,'Dovod sveže vode','vklop','=','0'),('Ponastavitev',8,'Izpust za odplake','vklop','=','0'),('Ponastavitev',9,'Dovod v pos. za prašek','vklop','=','0'),('Ponastavitev',10,'Gretje vode','vklop','=','0'),('Ponastavitev',11,'Ponastavitev','vklop','=','0');
/*!40000 ALTER TABLE `programi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-15 15:43:24
