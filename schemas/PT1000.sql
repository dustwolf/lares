-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lares
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `okvircki`
--

LOCK TABLES `okvircki` WRITE;
/*!40000 ALTER TABLE `okvircki` DISABLE KEYS */;
INSERT INTO `okvircki` VALUES ('Graf temperature','graf','{\"vklop\":\"1\",\"os-Y\":\"\\u010cas\",\"crta-A\":\"Temperatura\",\"crta-B\":\"\",\"crta-C\":\"\",\"crta-D\":\"\",\"trenutni\":\"grafi\\/Graf temperature.rrd\",\"zacetek\":0,\"modulA\":\"Temperatura\",\"modulB\":\"0\",\"modulC\":\"0\",\"modulD\":\"0\",\"parameterA\":\"vrednost\",\"parameterB\":\"0\",\"parameterC\":\"0\",\"parameterD\":\"0\",\"barvaA\":\"#ff0000\",\"barvaB\":\"#000000\",\"barvaC\":\"#000000\",\"barvaD\":\"#000000\",\"vrednostA\":20.131802120141,\"vrednostB\":\"U\",\"vrednostC\":\"U\",\"vrednostD\":\"U\",\"zadnjiCas\":1444132068,\"obdobje\":\"-15min\",\"id\":\"Graf temperature\",\"naslov\":\"Graf temperature\"}'),('Izklop','programator','{\"vklop\":\"0\",\"ime\":\"\",\"timer-stanje\":-6,\"timer\":\"0\",\"korak\":\"1\",\"timer-zadnjic\":1444132067,\"id\":\"Izklop\",\"naslov\":\"Izklop\"}'),('Regulator temperature','regulator','{\"vklop\":\"1\",\"modulSens\":\"Temperatura\",\"parameterSens\":\"vrednost\",\"vrednost\":20.131802120141,\"modulAct\":\"Ventil 2 - RO waste\",\"parameterAct\":\"vklop\",\"spodnja-meja\":\"24\",\"zgornja-meja\":\"26\",\"vrednostPod\":0,\"vrednostNad\":0,\"id\":\"Regulator temperature\",\"naslov\":\"Regulator temperature\",\"signal-pod\":\"0\",\"signal-ok\":\"0\",\"signal-nad\":\"1\"}'),('Senzor #1','digitalni senzor','{\"vklop\":\"1\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"port\":\"0\",\"stanje\":0,\"demo\":\"0\",\"id\":\"Senzor #1\",\"naslov\":\"Senzor #1\",\"vrednost\":\"0\"}'),('Temperatura','senzor','{\"vklop\":\"1\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"port\":\"4\",\"raw\":\"92\",\"vrednost\":20.131802120141,\"k\":0.10353356890459,\"n\":10.606713780919,\"vrednost1\":\"83\",\"vrednost2\":\"155\",\"demo\":\"0\",\"Kalibracijska-tocka-1\":\"19.2\",\"Kalibracijska-tocka-2\":\"48.5\",\"id\":\"Temperatura\",\"naslov\":\"Temperatura\"}'),('Ventil 1 - vstopni','ventil','{\"vklop\":\"0\",\"port\":\"0\",\"id\":\"Ventil 1 - vstopni\",\"naslov\":\"Ventil 1 - vstopni\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Ventil 2 - RO waste','ventil','{\"vklop\":\"0\",\"port\":\"1\",\"id\":\"Ventil 2 - RO waste\",\"naslov\":\"Ventil 2 - RO waste\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Ventil 3 - IIS vstopni','ventil','{\"vklop\":\"0\",\"port\":\"2\",\"id\":\"Ventil 3 - IIS vstopni\",\"naslov\":\"Ventil 3 - IIS vstopni\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Ventil 4 - IIS waste','ventil','{\"vklop\":0,\"port\":\"3\",\"id\":\"Ventil 4 - IIS waste\",\"naslov\":\"Ventil 4 - IIS waste\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Ventil 5 - končni','ventil','{\"vklop\":0,\"port\":\"4\",\"id\":\"Ventil 5 - kon\\u010dni\",\"naslov\":\"Ventil 5 - kon\\u010dni\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"sinrhonizacija\":\"Preklop\",\"stanje\":\"0\"}'),('Vklop','programator','{\"vklop\":\"0\",\"ime\":\"\",\"timer-stanje\":-21,\"timer\":\"-11\",\"korak\":\"1\",\"timer-zadnjic\":1444132067,\"id\":\"Vklop\",\"naslov\":\"Vklop\"}');
/*!40000 ALTER TABLE `okvircki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `preveri`
--

LOCK TABLES `preveri` WRITE;
/*!40000 ALTER TABLE `preveri` DISABLE KEYS */;
/*!40000 ALTER TABLE `preveri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `programi`
--

LOCK TABLES `programi` WRITE;
/*!40000 ALTER TABLE `programi` DISABLE KEYS */;
INSERT INTO `programi` VALUES ('Izklop',1,'Temperatura','vrednost','<','20'),('Izklop',2,'Ventil 3 - IIS vstopni','vklop','=','0'),('Vklop',1,'Temperatura','vrednost','>','20'),('Vklop',2,'Ventil 3 - IIS vstopni','vklop','=','1');
/*!40000 ALTER TABLE `programi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-06 13:47:48
