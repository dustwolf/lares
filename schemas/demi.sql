-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lares
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `okvircki`
--

LOCK TABLES `okvircki` WRITE;
/*!40000 ALTER TABLE `okvircki` DISABLE KEYS */;
INSERT INTO `okvircki` VALUES ('Prevodnost - končna','senzor','{\"vklop\":\"1\",\"port\":\"\",\"enota\":\"\",\"vrednost\":0,\"demo\":\"0\",\"demo-vrednost\":\"0\",\"id\":\"Prevodnost - kon\\u010dna\",\"naslov\":\"Prevodnost - kon\\u010dna\"}'),('Prevodnost - vstopna','senzor','{\"vklop\":\"1\",\"port\":\"\",\"enota\":\"\",\"vrednost\":0,\"demo\":\"0\",\"demo-vrednost\":\"0\",\"id\":\"Prevodnost - vstopna\",\"naslov\":\"Prevodnost - vstopna\"}'),('Tlak - ogljeni filter','senzor','{\"vklop\":\"1\",\"port\":\"\",\"enota\":\"\",\"vrednost\":0,\"demo\":\"0\",\"demo-vrednost\":\"0\",\"id\":\"Tlak - ogljeni filter\",\"naslov\":\"Tlak - ogljeni filter\"}'),('Tlak - trdni delci','senzor','{\"vklop\":\"1\",\"port\":\"\",\"enota\":\"\",\"vrednost\":0,\"demo\":\"0\",\"demo-vrednost\":\"0\",\"id\":\"Tlak - trdni delci\",\"naslov\":\"Tlak - trdni delci\"}'),('Tlak - vstopni','senzor','{\"vklop\":\"1\",\"port\":\"\",\"enota\":\"\",\"vrednost\":0,\"demo\":\"0\",\"demo-vrednost\":\"0\",\"id\":\"Tlak - vstopni\",\"naslov\":\"Tlak - vstopni\"}'),('Ventil 1 - vstopni','ventil','{\"vklop\":0,\"port\":\"\",\"id\":\"Ventil 1\",\"naslov\":\"Ventil 1 - vstopni\"}'),('Ventil 2 - RO waste','ventil','{\"vklop\":0,\"port\":\"\",\"id\":\"Ventil 2\",\"naslov\":\"Ventil 2 - RO waste\"}'),('Ventil 3 - IIS vstopni','ventil','{\"vklop\":0,\"port\":\"\",\"id\":\"Ventil 3\",\"naslov\":\"Ventil 3 - IIS vstopni\"}'),('Ventil 4 - IIS waste','ventil','{\"vklop\":0,\"port\":\"\",\"id\":\"Ventil 4\",\"naslov\":\"Ventil 4 - IIS waste\"}'),('Ventil 5 - končni','ventil','{\"vklop\":0,\"port\":\"\",\"id\":\"Ventil 5\",\"naslov\":\"Ventil 5 - kon\\u010dni\"}');
/*!40000 ALTER TABLE `okvircki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `programi`
--

LOCK TABLES `programi` WRITE;
/*!40000 ALTER TABLE `programi` DISABLE KEYS */;
/*!40000 ALTER TABLE `programi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-15 16:43:07
