-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lares
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `okvircki`
--

LOCK TABLES `okvircki` WRITE;
/*!40000 ALTER TABLE `okvircki` DISABLE KEYS */;
INSERT INTO `okvircki` VALUES ('Combo 1','combo','{\"vklop\":1,\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"stanje\":\"Povezan\",\"id\":\"Combo 1\",\"naslov\":\"Combo 1\",\"rele1\":false,\"rele2\":false,\"rele3\":false,\"rele4\":false,\"rele5\":false,\"kontakt\":[\"1\"],\"digital\":false,\"stikalo1\":false,\"stikalo2\":false,\"stikalo3\":false,\"stikalo4\":false,\"V\":2.6,\"A\":5.92,\"analog1\":0,\"analog2\":0.03,\"PT1000\":false,\"rele1-stanje\":false,\"rele2-stanje\":false,\"rele3-stanje\":false,\"rele4-stanje\":false,\"rele5-stanje\":false,\"digital-stanje\":false,\"glavni\":\"0\",\"pomozni\":\"0\"}'),('Nov ventil','ventil','{\"vklop\":\"0\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"port\":\"0\",\"stanje\":null,\"id\":\"Nov ventil\",\"naslov\":\"Nov ventil\",\"oprema\":0,\"rele\":0}'),('Senzor #1','digitalni senzor','{\"vklop\":\"1\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"port\":\"0\",\"stanje\":0,\"demo\":\"0\",\"id\":\"Senzor #1\",\"naslov\":\"Senzor #1\",\"vrednost\":false,\"oprema\":\"Combo 1\",\"tipalo\":\"stikalo1\"}'),('Senzor #2','digitalni senzor','{\"vklop\":\"1\",\"geslo\":\"admin\",\"IP\":\"ProtoSw1\",\"port\":\"1\",\"vrednost\":false,\"demo\":\"0\",\"id\":\"Senzor #2\",\"naslov\":\"Senzor #2\",\"oprema\":\"Combo 1\",\"tipalo\":\"stikalo2\"}'),('Stanje','panel','{\"vklop\":1,\"Panel-A\":\"Ni kontakta; danger\",\"Panel-B\":\"Zaprt; danger\",\"Panel-C\":\"0\",\"Panel-D\":\"0\",\"id\":\"Stanje\",\"naslov\":\"Stanje\"}'),('Stanje senzorja','regulator','{\"vklop\":\"1\",\"modulSens\":\"Senzor #1\",\"parameterSens\":\"vrednost\",\"vrednost\":false,\"modulAct\":\"Stanje\",\"parameterAct\":\"Panel-A\",\"signal\":\"Ni kontakta; danger\",\"spodnja-meja\":\"0\",\"zgornja-meja\":\"1\",\"signal-pod\":\"Ni kontakta; danger\",\"signal-ok\":\"Kontakt je; success\",\"signal-nad\":\"Kontakt je; success\",\"id\":\"Stanje senzorja\",\"naslov\":\"Stanje senzorja\"}'),('Stanje ventila','regulator','{\"vklop\":\"1\",\"modulSens\":\"Senzor #1\",\"parameterSens\":\"vrednost\",\"vrednost\":false,\"modulAct\":\"Stanje\",\"parameterAct\":\"Panel-B\",\"signal\":\"Zaprt; danger\",\"spodnja-meja\":\"0\",\"zgornja-meja\":\"1\",\"signal-pod\":\"Zaprt; danger\",\"signal-ok\":\"Odprt; success\",\"signal-nad\":\"Odprt; success\",\"id\":\"Stanje ventila\",\"naslov\":\"Stanje ventila\"}');
/*!40000 ALTER TABLE `okvircki` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `preveri`
--

LOCK TABLES `preveri` WRITE;
/*!40000 ALTER TABLE `preveri` DISABLE KEYS */;
/*!40000 ALTER TABLE `preveri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `programi`
--

LOCK TABLES `programi` WRITE;
/*!40000 ALTER TABLE `programi` DISABLE KEYS */;
INSERT INTO `programi` VALUES ('Izklop',1,'Senzor #1','vrednost','<','1'),('Izklop',2,'Ventil 1 - vstopni','vklop','=','0'),('Odpri vse ventile',1,'Ventil 1 - vstopni','vklop','=','1'),('Odpri vse ventile',2,'Ventil 2 - RO waste','vklop','=','1'),('Odpri vse ventile',3,'Ventil 3 - IIS vstopni','vklop','=','1'),('Odpri vse ventile',4,'Ventil 4 - IIS waste','vklop','=','1'),('Odpri vse ventile',5,'Ventil 5 - končni','vklop','=','1'),('Odpri vse ventile',6,'Odpri vse ventile','vklop','=','0'),('Vklop',1,'Senzor #1','vrednost','>','0'),('Vklop',2,'Ventil 1 - vstopni','vklop','=','1'),('Zapri vse ventile',1,'Ventil 1 - vstopni','vklop','=','0'),('Zapri vse ventile',2,'Ventil 2 - RO waste','vklop','=','0'),('Zapri vse ventile',3,'Ventil 3 - IIS vstopni','vklop','=','0'),('Zapri vse ventile',4,'Ventil 4 - IIS waste','vklop','=','0'),('Zapri vse ventile',5,'Ventil 5 - končni','vklop','=','0'),('Zapri vse ventile',6,'Zapri vse ventile','vklop','=','0');
/*!40000 ALTER TABLE `programi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-30  1:35:53
