<?php

/*

 Tale zadeva naj bi imela:
  * Tipke, ki na klik sprožijo določeno stanje v določenem modulu (4)
  * Napis "stanje sistema", ki izpisuje diagnozo avtomatskih diagnostik, oziroma kaj sistem trenutno dela
  * Izpis stanja posameznih izbranih modulov (2)

*/

$common->tip = "proces";
$common->ikona = "digitalni";
$common->ajaxRefresh = True;

$common->nastavitve = array();
$common->runtime = array(
 "Panel-A",
 "Panel-B",
 "Panel-C",
 "Panel-D"
);
$common->podatki = array(
 "vklop"
);

?>
