<?php

 if($velikost == "mega") { echo "<h3>"; }

 $panels = array(
  trim($this->stanje->{"Panel-A"}),
  trim($this->stanje->{"Panel-B"}),
  trim($this->stanje->{"Panel-C"}),
  trim($this->stanje->{"Panel-D"})
 );

 foreach($panels as $panel) {
  if($panel != "" && $panel != "0") {
 
   if(strpos($panel, ";") !== False) {
    $barva = substr($panel, strpos($panel, ";")+1);
    $panel = substr($panel, 0, strpos($panel, ";"));
   } else {
    $barva = "default";
   }

   ?>
    <label class="label label-<?php echo trim($barva); ?>">
     <?php echo $panel; ?>
    </label>&nbsp;
   <?php

  }
 }

 if($velikost == "mega") { echo "</h3>"; }


?>
