<?php

if($this->stanje->demo == 1) {

 $this->stanje->vrednost = round(rand(0,100));

} else {

 require_once "engines.php";
 $motor = new motor($this->stanje->oprema);
 $vrednost = $motor->stanje->{$this->stanje->tipalo};
 unset($motor);

 $this->stanje->raw = $vrednost;
 $this->stanje->vrednost = $vrednost * $this->stanje->k + $this->stanje->n;

}

?>
