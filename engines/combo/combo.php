<?php

class combo {

 public $stanje;
 public $napaka;

 public $releji;

 private $geslo;
 private $IP;

 function __construct($geslo, $IP, $releji = array(0,0,0,0,0,0)) {
  $this->geslo = $geslo;
  $this->IP = $IP;

  $this->releji = $releji;
  
  $this->get();
 }

 function get($force = False) {

  if(!isset($this->stanje->updated) || $this->stanje->updated < time() || $force) {
   exec(
    "curl -s --connect-timeout 1 ".
    "http://admin:".escapeshellarg($this->geslo).
                "@".escapeshellarg($this->IP)."/st0.xml"
   ,$xml);
 
   $xml = implode("",$xml);
   $this->napaka = ($xml === "");
   if(!$this->napaka) {
    $this->stanje = simplexml_load_string($xml);
    $this->stanje->updated = time();
   }
 
  }

 }

 function set() {

  if(!$this->napaka) {
   $this->get();

   foreach($this->releji as $r => $s) {

    if($this->stanje->{"out".$r} == $s) {
     if($s) {
      $out = 0;
     } else {
      $out = 1;
     }

     exec("curl -s --connect-timeout 1 "
          .escapeshellarg("http://admin:".$this->geslo."@".$this->IP.
                          "/outs.cgi?out".rawurlencode($r)."=".rawurlencode($out)));
    }
   }

   $this->get(True);

  }
 }

 function __destruct() {
  $this->set();
 }

}

?>
