<?php

require_once "engines/combo/combo.php";

$s = $this->stanje;
$releji = array($s->rele1,$s->rele2,$s->rele3,$s->rele4,$s->rele5,$s->digital);
unset($s);

$m = new combo($this->stanje->geslo, $this->stanje->IP, $releji);

if($m->napaka) {

 $this->stanje->vklop = 0;
 $this->stanje->stanje = "Napaka!";

} else {

 $this->stanje->vklop = 1;
 $this->stanje->stanje = "Povezan";
 
 $this->stanje->{"rele1-stanje"} = ($m->stanje->out0 == 0);
 $this->stanje->{"rele2-stanje"} = ($m->stanje->out1 == 0);
 $this->stanje->{"rele3-stanje"} = ($m->stanje->out2 == 0);
 $this->stanje->{"rele4-stanje"} = ($m->stanje->out3 == 0);
 $this->stanje->{"rele5-stanje"} = ($m->stanje->out4 == 0);
 $this->stanje->{"digital-stanje"} = ($m->stanje->out5 == 0);
 $this->stanje->stikalo1 = ($m->stanje->di0 != "up");
 $this->stanje->stikalo2 = ($m->stanje->di1 != "up");
 $this->stanje->stikalo3 = ($m->stanje->di2 != "up");
 $this->stanje->stikalo4 = ($m->stanje->di3 != "up");
 $this->stanje->analog1 = $m->stanje->ia2 / 100;
 $this->stanje->analog2 = $m->stanje->ia3 / 100;
 if($m->stanje->ia4 != 4326) {
  $this->stanje->PT1000 = $m->stanje->ia4 / 10;
 } else {
  $this->stanje->PT1000 = false;
 }
 $this->stanje->V = $m->stanje->ia6 / 10;
 $this->stanje->A = $m->stanje->ia5 / 100;

 if($this->stanje->pomozni == 1) {
  $this->stanje->rele1 = $this->stanje->{"rele1-stanje"};
  $this->stanje->rele2 = $this->stanje->{"rele2-stanje"};
  $this->stanje->rele3 = $this->stanje->{"rele3-stanje"};
  $this->stanje->rele4 = $this->stanje->{"rele4-stanje"};
  $this->stanje->rele5 = $this->stanje->{"rele5-stanje"};
  $this->stanje->digital = $this->stanje->{"digital-stanje"};
 }

 $s = $this->stanje;
 $m->releji = array($s->rele1,$s->rele2,$s->rele3,$s->rele4,$s->rele5,$s->digital);
 unset($s);

}

unset($m);

?>
