<?php

$common->tip = "oprema";
$common->ikona = "motor";

$common->runtime = array(
 "vklop",
 "rele1", "rele2", "rele3", "rele4", "rele5", "digital",
);
$common->podatki = array(
 "stanje", 
 "rele1-stanje", "rele2-stanje", "rele3-stanje", "rele4-stanje", "rele5-stanje", "digital-stanje",
 "stikalo1", "stikalo2", "stikalo3", "stikalo4",
 "analog1", "analog2", "PT1000", "A", "V"
);
$common->nastavitve = array(
 "geslo", 
 "IP",
 "pomozni"
);

?>
