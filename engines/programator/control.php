<?php

/*

 TODO:
  * Tole mislim da je:
   ** Za vsako stvar ki jo programator naredi, zabeleži ali je potem treba čakati ali ne
   ** Čaka se za timer, za neizpolnjene pogoje in za restart programatorja
   ** okrog celega programatorja naredi zanko, ki se ponavlja dokler ni potrebno čakanje

*/

//TRIGGER SPROŽI PROGRAMATOR SAMO OB PREHODU IZ 0 V 1
if($this->stanje->{"trigger"} != $this->stanje->{"ex-trigger"}) {

 $set = array();
 if($this->stanje->{"trigger"} == 1) {
  $set["vklop"] = 1;
 }

 $set["ex-trigger"] = $this->stanje->{"trigger"};
 $this->nastavi($set);

}


if($this->stanje->vklop == 1) {

 $wait = False;
 require_once "mysqli.php";
 $db = new dblink();

 while(!$wait) {

  $wait = True;

  //odštevaj timer
  if(isset($this->stanje->{"timer-zadnjic"}) && $this->stanje->{"timer-zadnjic"} > 0) {
  
   $elapsed = time() - $this->stanje->{"timer-zadnjic"};
    
   $this->stanje->timer = $this->stanje->timer - $elapsed;
   $this->stanje->{"timer-stanje"} = $this->stanje->timer;
 
  }
 
  $tmp = $db->q("
   SELECT `vrstica` FROM `programi`
    WHERE `programator` = '".$db->e($this->naslov)."'
    ORDER BY `vrstica` DESC
    LIMIT 1
  ");

  if(isset($tmp[0]["vrstica"])) {
   $zadnjaVrstica = $tmp[0]["vrstica"];
 
   //če si opravil cel program, začni od začetka
   if($this->stanje->korak > $zadnjaVrstica) {
    $this->stanje->korak = 0;
    $this->stanje->timer = 0; //nastavi timer na 0
    $wait = True;
   }

  } else {

   //če programa ni, se resetiraj in ugasni
   $this->stanje->korak = 0;
   $this->stanje->vklop = 0;
   $wait = True;

  } 

  //izvajaj program
  $program = $db->q("
   SELECT `modul`, `parameter`, `operator`, `vrednost`
     FROM `programi`
    WHERE `programator` = '".$db->e($this->naslov)."'
      AND `vrstica` = '".$db->e($this->stanje->korak)."'
     LIMIT 1
  ");

  if(count($program) == 0) {
 
   //če na tej vrstici ni programa, ga preskoči
   $this->stanje->korak++;
 
  } else {

   if($program[0]["operator"] == "=") {

    //če je to set ukaz, nastavi vrednost
    if($program[0]["modul"] == $this->naslov) {
 
     $this->nastavi(array($program[0]["parameter"] => $program[0]["vrednost"]));
     $this->stanje->{"timer-stanje"} = $this->stanje->timer;

     //...in pojdi na naslednji korak razen če sem v tem koraku spremenil korak
     if($program[0]["parameter"] != "korak") {
      $this->stanje->korak++;
      $wait = False;
     }

    } else {

     $motor = new motor($program[0]["modul"]);
     $motor->nastavi(array($program[0]["parameter"] => $program[0]["vrednost"]));    

     //...in pojdi na naslednji korak
     $this->stanje->korak++;
     $wait = False;

    }
 
   } elseif($program[0]["operator"] == ">") {

    //če je to if ukaz, preveri stanje in nadaljuj če je izpolnjeno
    $motor = new motor($program[0]["modul"]);
    $wait = True;
    if($motor->stanje->{$program[0]["parameter"]} != "napaka!" && $motor->stanje->{$program[0]["parameter"]} > $program[0]["vrednost"]) {
     $this->stanje->korak++;
     $wait = False;
    }

   } elseif($program[0]["operator"] == "<") {

    //če je to if ukaz, preveri stanje in nadaljuj če je izpolnjeno
    $motor = new motor($program[0]["modul"]);
    $wait = True;
    if($motor->stanje->{$program[0]["parameter"]} != "napaka!" && $motor->stanje->{$program[0]["parameter"]} < $program[0]["vrednost"]) {
     $this->stanje->korak++;
     $wait = False;
    }   
   }

  }

 }
 
}

//beleži čas od zadnjega cikla za timer, tudi če je odštevanje ugasnjeno
$this->stanje->{"timer-zadnjic"} = time();

?>
