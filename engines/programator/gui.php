<p>Korak: <?php echo $stanje->korak; ?></p>
<?php
 
 /*


 */

 if($velikost == "mega") {

  if(isset($_POST["programator"])) {

   require_once "mysqli.php";
   $db = new dblink();
   
   $db->q("
    INSERT INTO `programi` 
     (`programator`, `vrstica`, `modul`, `parameter`, `operator`, `vrednost`)
     VALUES ('".$db->e($_POST["programator"])."',
             '".$db->e($_POST["vrstica"])."',
             '".$db->e($_POST["modul"])."',
             '".$db->e($_POST["parameter"])."',
             '".$db->e($_POST["operator"])."',
             '".$db->e($_POST["vrednost"])."')
    ON DUPLICATE KEY UPDATE
     `modul` = '".$db->e($_POST["modul"])."',
     `parameter` = '".$db->e($_POST["parameter"])."',
     `operator` = '".$db->e($_POST["operator"])."',
     `vrednost` = '".$db->e($_POST["vrednost"])."'
   ");
   
  }

  require_once "mysqli.php";
  $db = new dblink();

  $uredi = 0;
  if(isset($_GET["urediProg"])) {

   $uredi = $_GET["urediProg"];
   ?><script>window.urejaj = true</script><?php

  } elseif(isset($_GET["brisiProg"])) {

   $db->q("
    DELETE FROM `programi`
     WHERE `programator` = '".$db->e($this->naslov)."' 
       AND `vrstica` = '".$db->e($_GET["brisiProg"])."'
   ");

  }

 ?>
 <form method="POST" action="index.php?fokus=<?php echo rawurlencode($this->naslov); ?>">
  <input type="hidden" name="programator" value="<?php echo $this->naslov; ?>">
  <table class="table">
   <thead>
    <tr>
     <th>#</th>
     <th>Modul</th>
     <th>Parameter</th>
     <th>Operator</th>
     <th>Vrednost</th>
     <th class="urejaj-table-cell">Uredi</th>
    </tr>
   </thead>
   <tbody>
  <?php

   if($uredi == 0) {
    $tmp = $db->q("
     SELECT `vrstica` FROM `programi`
      WHERE `programator` = '".$db->e($this->naslov)."'
      ORDER BY `vrstica` DESC
      LIMIT 1  
    ");
  
    if(isset($tmp[0])) { 
     $uredi = $tmp[0]["vrstica"] + 1;
    } else {
     $uredi = 1;
    } 
   }
   
   $vrstice = $db->q("
    SELECT `vrstica`, `modul`, `parameter`, `operator`, `vrednost`
     FROM `programi`
     WHERE `programator` = '".$db->e($this->naslov)."'
   ", "vrstica");

   foreach($vrstice as $vrstica) {
    if($vrstica["vrstica"] == $uredi) {
     $staro = $vrstica;
    } else {

    ?>
     <tr style="<?php if($vrstica['vrstica'] == $this->stanje->korak) { echo 'background-color: lime;'; } ?>">
      <td><?php echo $vrstica["vrstica"]; ?></td>
      <td><?php echo $vrstica["modul"]; ?></td>
      <td><?php echo $vrstica["parameter"]; ?></td>
      <td><?php echo $vrstica["operator"]; ?></td>
      <td><?php echo $vrstica["vrednost"]; ?></td> 
      <td class="urejaj-table-cell">
       <a class="btn btn-warning" href="?urediProg=<?php echo rawurlencode($vrstica['vrstica']); 
                                                          ?>&fokus=<?php echo rawurlencode($this->naslov); ?>">Uredi</a>&nbsp;
       <a class="btn btn-danger" href="?brisiProg=<?php echo rawurlencode($vrstica['vrstica']); 
                                                         ?>&fokus=<?php echo rawurlencode($this->naslov); ?>">Briši</a>
      </td>
     </tr> 
    <?php
    }
   }

   if(!isset($staro)) {
    $staro["vrstica"] = $uredi;
    $staro["modul"] = "-";
    $staro["parameter"] = "-";
    $staro["operator"] = "-";
    $staro["vrednost"] = "";
   }
  ?>
    <tr class="urejaj-table-row">
     <td><input type="hidden" name="vrstica" value="<?php echo $uredi; ?>"></td>
     <td>      
      <select class="form-control" name="modul" id="modul-picker">
       <option value="<?php echo $staro['modul']; ?>" selected><?php echo $staro['modul']; ?></option>
       <?php

        $moduli = $db->q("
         SELECT `naslov` FROM `okvircki`
          WHERE `naslov` NOT IN ('".$db->e($staro["modul"])."')
        ","naslov");
 
        foreach ($moduli as $modul) { 
        ?>
        <option value="<?php echo $modul['naslov']; ?>" <?php if($staro["modul"] == $modul["naslov"]) { echo "selected"; } ?>>
         <?php echo $modul['naslov']; ?>
        </option>
        <?php
        }
       ?>
      </select>
     </td>
     <td>
      <select class="form-control" name="parameter" id="parameter-picker">
       <option value="<?php echo $staro['parameter']; ?>" selected><?php echo $staro['parameter']; ?></option>
      </select>
     </td>
     <td>
      <select class="form-control" name="operator" id="operator-picker">
       <option value="<?php echo $staro['operator']; ?>" selected><?php echo $staro['operator']; ?></option>
      </select>
     </td>
     <td><input class="form-control" name="vrednost" type="text" value="<?php echo $staro['vrednost']; ?>"></td>
     <td><input class="form-control btn btn-success" type="submit" value="vnos"></td>
    </tr>
   </tbody>
  </table>
 </form>

 <script>
  jQuery("#modul-picker").on("click", function() {
   window.tmp = jQuery("#parameter-picker").val();
   jQuery.ajax("engines/programator/ajax.php?modul="+encodeURIComponent(this.value)).done(
    function(data) { jQuery("#parameter-picker").html(data); jQuery("#parameter-picker").val(window.tmp); }
   );    
  });
  jQuery("#parameter-picker").on("click", function() {
   window.tmp = jQuery("#operator-picker").val();
   jQuery.ajax("engines/programator/ajax.php?modul="+encodeURIComponent(jQuery("#modul-picker").val())
              +"&parameter="+encodeURIComponent(this.value)).done(
    function(data) { jQuery("#operator-picker").html(data); jQuery("#operator-picker").val(window.tmp);}
   );    
  });
 </script>
 <?php
 }
?>
