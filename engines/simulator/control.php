<?php

 if($this->stanje->vklop == 1) {

  require_once "engines.php";
  
  $dovod = new motor("Dovod sveže vode");
  if($dovod->stanje->vklop == 1) {
   $gladina = new motor("Gladina vode");
   $gladina->nastavi(array("demo" => "1", "demo-vrednost" => "600"));
  }

  $izpust = new motor("Izpust za odplake");
  $crpalka = new motor("Črpalka");
  if($izpust->stanje->vklop == 1 && $crpalka->stanje->vklop == 1) {
   $gladina = new motor("Gladina vode");
   $gladina->nastavi(array("demo" => "1", "demo-vrednost" => "10"));   
  }

  $gretje = new motor("Gretje vode");
  $temperatura = new motor("Temperatura vode");
  if($gretje->stanje->vklop == 1) {
   $temperatura->nastavi(array("demo" => "1", "demo-vrednost" => $gretje->stanje->nastavitev));
  } else {
   $temperatura->nastavi(array("demo" => "1", "demo-vrednost" => "22.5"));   
  }

 }

?>
