<?php if($this->stanje->vklop == 1) { echo "Obratuje"; } else { echo "Ustavljen"; } ?></p>
<?php

 if($this->stanje->vklop == 1 && $velikost == "mega") {

  require_once "engines.php";
  
  $dovod = new motor("Dovod sveže vode");
  if($dovod->stanje->vklop == 1) {
   ?><span class="label label-primary">Dotok vode</span> <?php
  }

  $izpust = new motor("Izpust za odplake");
  $crpalka = new motor("Črpalka");
  if($izpust->stanje->vklop == 1 && $crpalka->stanje->vklop == 1) {
   ?><span class="label label-primary">Odtok vode</span> <?php
  }

  $gretje = new motor("Gretje vode");
  if($gretje->stanje->vklop == 1) {
   ?><span class="label label-primary">Gretje vode</span> <?php
  } else {
   ?><span class="label label-primary">Ohlajanje vode</span> <?php
  }

 }

?><br><br>
