<?php

if($this->stanje->demo == 1) {

 $this->stanje->vrednost = round(rand(0,1));

} else {

 require_once "engines.php";
 $motor = new motor($this->stanje->oprema);
 if($this->stanje->tipalo != "0") {
  $this->stanje->vrednost = ($motor->stanje->{$this->stanje->tipalo} == 1);
 } else {
  $this->stanje->vrednost = False;
 }
 unset($motor);

}

?>
