<?php

 if($velikost == "mega") {

  echo $this->stanje->modulSens.", ".$this->stanje->parameterSens." = ".$this->stanje->vrednost."<br>";

  echo "&rarr; ".$this->stanje->modulAct.", ".$this->stanje->{"parameterAct"}."<br><br>";

  echo "&lt; ".$this->stanje->{"spodnja-meja"}." &rarr; ".$this->stanje->{"signal-pod"}.".<br>";
  echo "OK &rarr; ".$this->stanje->{"signal-ok"}.".<br>";
  echo "&ge; ".$this->stanje->{"zgornja-meja"}." &rarr; ".$this->stanje->{"signal-nad"}.".<br>";


 } else {

  echo $this->stanje->modulSens.", ".$this->stanje->parameterSens.":<br>";

  echo round($this->stanje->{"spodnja-meja"},2)." &lt; ".
       round($this->stanje->vrednost,2)." &le; ".
       round($this->stanje->{"zgornja-meja"},2); 

 }

?>
