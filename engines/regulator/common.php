<?php

$common->tip = "proces";
$common->ikona = "temperatura";

$common->runtime = array(
 "vklop",
 "spodnja-meja", "zgornja-meja",
 "signal-pod", "signal-ok", "signal-nad"
);
$common->podatki = array(
 "modulSens", "parameterSens", "vrednost",
 "modulAct", "parameterAct",
 "signal"
);

/*

 TODO
  * Tole predelaj v modul ki sprejema:
    * Modul in parameter za spremljanje
    * Zgornjo in spodnjo mejo
    * Modul in parameter za nastavljanje
    * Vrednost za in-band in vrednost za out-of-band
  * Kadar je vhodni podatek v mejah, pošilja eno, sicer drugo

  * spremeni iz dveh aktuatorjev v enega samega
  * poleg vrednostNad in vrednostPod dodaj še srednjo vrednost

*/

?>
