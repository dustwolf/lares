<?php

/*

 TODO:
  * Tale zadeva naj preverja v kakem razponu je en senzor
  * Če je zunaj nastavi eno stanje enega modula
  * Če je znotraj nastavi drugo stanje drugega (ali istega) modula

*/

require_once "engines.php";

$senzor = new motor($this->stanje->modulSens);
$this->stanje->vrednost = $senzor->stanje->{$this->stanje->parameterSens};

if($this->stanje->vklop == 1) {

 if($this->stanje->vrednost <= $this->stanje->{"spodnja-meja"}) {
  $signal = $this->stanje->{"signal-pod"};
 } elseif($this->stanje->vrednost > $this->stanje->{"zgornja-meja"}) {
  $signal = $this->stanje->{"signal-nad"};
 } else {
  $signal = $this->stanje->{"signal-ok"};
 }

 $aktuator = new motor($this->stanje->modulAct);

 if($aktuator->stanje->{$this->stanje->parameterAct} != $signal) {
  $aktuator->nastavi(array(
   $this->stanje->parameterAct => $signal
  ));
 }

 $this->nastavi(array(
  "signal" => $signal
 ));
 
}

?>
