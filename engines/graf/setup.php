<?php

if("graphs/".$this->naslov.".rrd" !== $this->stanje->trenutni) {

 $this->stanje->trenutni = "graphs/".$this->naslov.".rrd";

 if(realpath(substr($this->stanje->trenutni, 0, strrpos($this->stanje->trenutni,"/"))) != realpath("graphs")) {
  $this->stanje->trenutni = "";
 } else {
  $this->nastavi(array("trenutni" => $this->stanje->trenutni));
 }

 if($this->stanje->trenutni != "") {
 
  $this->stanje->zacetek = time();
  $data = array("--start",$this->stanje->zacetek,
                "--step", 1,
                "DS:crtaA:GAUGE:1:U:U",
                "DS:crtaB:GAUGE:1:U:U",
                "DS:crtaC:GAUGE:1:U:U",
                "DS:crtaD:GAUGE:1:U:U",
                "RRA:AVERAGE:0.99:5:86400");
  rrd_create($this->stanje->trenutni, $data);
  $error = rrd_error();
  if($error != "") {
   ?>
    <span class="label label-danger"><?php echo $error; ?></span>&nbsp;
   <?php
  }

 } else {
?>
 <span class="label label-danger">Napaka v imenu grafa</span>&nbsp;
<?php
 }

}

?>
<label>barva črte A</label>
<input class="form-control" type="color" name="barvaA" value="<?php echo $this->stanje->barvaA; ?>">
<br>

<label>barva črte B</label>
<input class="form-control" type="color" name="barvaB" value="<?php echo $this->stanje->barvaB; ?>">
<br>

<label>barva črte C</label>
<input class="form-control" type="color" name="barvaC" value="<?php echo $this->stanje->barvaC; ?>">
<br>

<label>barva črte D</label>
<input class="form-control" type="color" name="barvaD" value="<?php echo $this->stanje->barvaD; ?>">
<br>

<?php
 
require_once "engines.php";
$picker = new modulPicker($this->doc, $this->stanje, "podatki");

foreach(array('A','B','C','D') as $crka) {
 $picker->moduli("modul za črto ".$crka, "modul".$crka);
 $picker->parametri("parameter za črto ".$crka, "parameter".$crka);
}

?>
