<?php

$common->tip = "proces";
$common->ikona = "graf";

//graf = filename, trenutni = filename za preverjanje ali se je spremenil
//zacetek = zacetek grafa v UNIX času, crta-A...D = napisi črt, os-X,Y = napisi osi
//obdobje = -1y
$common->nastavitve = array("os-Y", 
                            "crta-A", "crta-B", "crta-C","crta-D");

$common->runtime = array("vklop", "obdobje");

$common->podatki = array("trenutni", "zacetek", 
                         "modulA", "modulB", "modulC", "modulD", 
                         "parameterA", "parameterB", "parameterC", "parameterD",
                         "barvaA", "barvaB", "barvaC", "barvaD",
																									"vrednostA", "vrednostB", "vrednostC", "vrednostD",
                         "zadnjiCas");

?>
