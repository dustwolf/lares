<?php

 if($this->stanje->obdobje === 0) {
  $this->stanje->obdobje = "-3h";
 }

 if($this->stanje->vklop == 1) {

  //ČE TO SEKUNDO ŠE NISI POSODOBIL STANJA
  if($this->stanje->trenutni != "") {

   require_once "engines.php";   

   if($this->stanje->zadnjiCas != time()) {
    $this->stanje->zadnjiCas = time();

    if($this->stanje->modulA !== "0") {
     $motor = new motor($this->stanje->modulA);
     $this->stanje->vrednostA = $motor->stanje->{$this->stanje->parameterA};
    } else {
     $this->stanje->vrednostA =  "U";
    }

    if($this->stanje->modulB !== "0") {
     $motor = new motor($this->stanje->modulB);
     $this->stanje->vrednostB = $motor->stanje->{$this->stanje->parameterB};
    } else {
     $this->stanje->vrednostB = "U";
    }

    if($this->stanje->modulC !== "0") {
     $motor = new motor($this->stanje->modulC);
     $this->stanje->vrednostC = $motor->stanje->{$this->stanje->parameterC};
    } else {
     $this->stanje->vrednostC = "U";
    }

    if($this->stanje->modulD !== "0") {
     $motor = new motor($this->stanje->modulD);
     $this->stanje->vrednostD = $motor->stanje->{$this->stanje->parameterD};
    } else {
     $this->stanje->vrednostD = "U";
    }

    $this->nastavi(array(
     "vrednostA" => $this->stanje->vrednostA,
     "vrednostB" => $this->stanje->vrednostB,
     "vrednostC" => $this->stanje->vrednostC,
     "vrednostD" => $this->stanje->vrednostD
    ));

    $data = array(
     $this->stanje->zadnjiCas.":".$this->stanje->vrednostA.":".$this->stanje->vrednostB.":".$this->stanje->vrednostC.":".$this->stanje->vrednostD
    );
    
    rrd_update($this->stanje->trenutni, $data);
    echo rrd_error();

   }

  } else {
   $this->stanje->vklop = 0;
  }

 }

?>
