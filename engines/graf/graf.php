<?php

if(isset($_GET["ime"])) {

 require_once "../../engines.php";

 $motor = new motor($_GET["ime"]);

 $graf = "/tmp/".uniqid("lares-graf").".png";
 $data = array(
  "--start",$motor->stanje->obdobje,
  "--end","now",
  "--vertical-label", $motor->stanje->{"os-Y"});

 if($motor->stanje->modulA !== "0") {
  $data = array_merge($data,array(
   "DEF:crtaA=../../".$motor->stanje->trenutni.":crtaA:AVERAGE",
   'LINE1:crtaA'.$motor->stanje->barvaA.':"'.$motor->stanje->{"crta-A"}.'"'));
 }

 if($motor->stanje->modulB !== "0") {
  $data = array_merge($data,array(
   "DEF:crtaB=../../".$motor->stanje->trenutni.":crtaB:AVERAGE",
   'LINE2:crtaB'.$motor->stanje->barvaB.':"'.$motor->stanje->{"crta-B"}.'"'));
 }

 if($motor->stanje->modulC !== "0") {
  $data = array_merge($data,array(
   "DEF:crtaC=../../".$motor->stanje->trenutni.":crtaC:AVERAGE",
   'LINE3:crtaC'.$motor->stanje->barvaC.':"'.$motor->stanje->{"crta-C"}.'"'));
 }

 if($motor->stanje->modulD !== "0") {
  $data = array_merge($data,array(
   "DEF:crtaD=../../".$motor->stanje->trenutni.":crtaD:AVERAGE",
   'LINE4:crtaD'.$motor->stanje->barvaD.':"'.$motor->stanje->{"crta-D"}.'"'));
 }

 rrd_graph($graf, $data);

 header("Content-Type: image/png");
 header("Content-Length: " . filesize($graf));
 readfile($graf);

 unlink($graf);

}

?>
