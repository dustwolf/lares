<?php

if($velikost == "mega") {

 echo "<br>";
 require_once "engines.php";

 if(trim($this->stanje->{"Tipka-A"}) != "") {
  
  $motor = new motor($this->stanje->modulA);
  
  $motor->elementTipka(
   $this->stanje->{"Tipka-A"}, 
   $this->stanje->parameterA, 
   $this->stanje->vrednostA, 
   "primary btn-lg"
  );
 }

 if(trim($this->stanje->{"Tipka-B"}) != "") {
  
  $motor = new motor($this->stanje->modulB);
  
  $motor->elementTipka(
   $this->stanje->{"Tipka-B"}, 
   $this->stanje->parameterB, 
   $this->stanje->vrednostB, 
   "primary btn-lg"
  );
 }

 if(trim($this->stanje->{"Tipka-C"}) != "") {
  
  $motor = new motor($this->stanje->modulC);
  
  $motor->elementTipka(
   $this->stanje->{"Tipka-C"}, 
   $this->stanje->parameterC, 
   $this->stanje->vrednostC, 
   "primary btn-lg"
  );
 }

 if(trim($this->stanje->{"Tipka-D"}) != "") {
  
  $motor = new motor($this->stanje->modulD);
  
  $motor->elementTipka(
   $this->stanje->{"Tipka-D"}, 
   $this->stanje->parameterD, 
   $this->stanje->vrednostD, 
   "primary btn-lg"
  );
 }

} else {

echo trim(implode(", ",array(
 $this->stanje->{"Tipka-A"},
 $this->stanje->{"Tipka-B"},
 $this->stanje->{"Tipka-C"},
 $this->stanje->{"Tipka-D"}
)),", ");

}

?>
