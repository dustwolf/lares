<?php

/*

 Tale zadeva naj bi imela:
  * Tipke, ki na klik sprožijo določeno stanje v določenem modulu (4)
  * Napis "stanje sistema", ki izpisuje diagnozo avtomatskih diagnostik, oziroma kaj sistem trenutno dela
  * Izpis stanja posameznih izbranih modulov (2)

*/

$common->tip = "proces";
$common->ikona = "dejanje";
$common->ajaxRefresh = False;

$common->nastavitve = array(
 "Tipka-A", "vrednostA",  
 "Tipka-B", "vrednostB",  
 "Tipka-C", "vrednostC",
 "Tipka-D", "vrednostD"
);
$common->runtime = array();
$common->podatki = array(
 "vklop",
 "modulA", "parameterA", 
 "modulB", "parameterB",
 "modulC", "parameterC",
 "modulD", "parameterD"
);

?>
