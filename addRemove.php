<?php

/*

 TODO
  * Tale zadeva omogoča dodajanje in odstranjevanje okvirčkov v navezavi z povezanimi napravami

 TODO
  * ?fokus parameter mora sprožiti dodajanje / odstranjevanje
  * Ko se nek nov modul doda, se morajo v json stanju ustvariti $common->nastavitve

*/

require_once "html.php";
$doc = new html("LARES - Add or remove modules", array(
 "bootstrap" => True, 
 "css" => "style.css"
));

include "menu.php";

?><div class="body-padding"><?php

require_once "engines.php";

//PO POTREBI DODAJ ALI ODSTRANI OKVIRCKE
if(isset($_GET["fokus"])) {
 $okvircek = $_GET["fokus"];
 if(substr($okvircek,-1) == "#") {

  //dodaj
  $id = substr($okvircek,0,-2);
  $motor = new dodajMotor("Nov ".$id, $id);
  $motor->install();
  header("Location: settings.php?fokus=".rawurlencode("Nov ".$id));

 } else {

  //odstrani
  $motor = new odstraniMotor($okvircek);
  $motor->uninstall();

 }
}

require_once "mysqli.php";
$db = new dblink();

//NARISI OBSTOJECE OKVIRCKE BREZ FOKUSA
$okvircki = $db->q("
 SELECT `naslov`, `motor`, `stanje` 
  FROM `okvircki`
  ORDER BY `motor`
");

foreach($okvircki as $okvircek) {

 $motor = new motor($okvircek["naslov"], $okvircek["motor"], json_decode($okvircek["stanje"]), false, $doc);
 $motor->gui("Remove this ".$okvircek["motor"]);

}

//NARISI PRAZNE TIPKE ZA NOVE OKVIRCKE, za vse motorje ki so na voljo
$d = dir("engines");
while (false !== ($entry = $d->read())) {
 if($entry != "." && $entry != "..") {
  $motor = new dodajMotor($entry." #", $entry);
  $motor->gui();  
 }
}

?></div>
