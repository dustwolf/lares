<?php

  if($tip == "aktuator" && $aktivnost == 0) {
   $barva = "#ff2d2d";  //ustavljen = rdeč
  } elseif($tip == "aktuator" && $aktivnost == 1) {
   $barva = "#2dff5b";  //dela = zelen
  } elseif($tip == "senzor" && $aktivnost == 1) {
   $barva = "#2db4ff";  //n/a = moder
  } elseif($tip == "proces" && $aktivnost == 1) {
   $barva = "#fffd2d";  //proces = rumen
  } elseif($tip == "senzor" && $aktivnost == 0) {
   $barva = "#CCCCCC";  //neaktiven senzor = siv
  } elseif($tip == "proces" && $aktivnost == 0) {
   $barva = "#CCCCCC";  //neaktiven proces = siv
  } elseif($tip == "oprema" && $aktivnost == 1) {
   $barva = "#CC00CC";  //oprema = vijolična
  } elseif($tip == "oprema" && $aktivnost == 0) {
   $barva = "#ff2d2d";  //oprema napaka = rdeča
  }

  if($aktivnost == -1) {
   $barva = "#fff";
  }

?>
