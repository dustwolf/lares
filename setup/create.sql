CREATE TABLE `okvircki` (
  `naslov` varchar(45) NOT NULL,
  `motor` varchar(45) DEFAULT 'vzorec.php',
  `stanje` text,
  PRIMARY KEY (`naslov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `programi` (
  `programator` varchar(45) NOT NULL,
  `vrstica` int(11) NOT NULL,
  `modul` varchar(45) DEFAULT NULL,
  `parameter` varchar(45) DEFAULT NULL,
  `operator` varchar(10) DEFAULT NULL,
  `vrednost` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`programator`,`vrstica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `preveri` (
  `naslov` varchar(45) NOT NULL,
  PRIMARY KEY (`naslov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
