#!/usr/bin/php
<?php

require_once "../mysqli.php";
$db = new dblink();

//ČE SE NE GRE POVEZATI, SE PRIJAVI Z ROOTOM IN IZDELAJ BAZO IN UPORABNIKA
if($db->link->connect_error) {

 $db = new dblink("","root","password");

 $db->q("
  CREATE DATABASE `lares`
 ");

 $db->q("
  CREATE USER 'lares'@'localhost' IDENTIFIED BY 'password';
 ");

 $db->q("
  GRANT ALL PRIVILEGES ON `lares`.* TO 'lares'@'localhost'
 ");

 $db->q("
  FLUSH PRIVILEGES
 ");

}

$db = new dblink();

$db->q("
 CREATE TABLE IF NOT EXISTS `okvircki` (
   `naslov` varchar(45) NOT NULL,
   `motor` varchar(45) DEFAULT 'vzorec.php',
   `stanje` text,
   PRIMARY KEY (`naslov`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$db->q("
 CREATE TABLE IF NOT EXISTS `programi` (
   `programator` varchar(45) NOT NULL,
   `vrstica` int(11) NOT NULL,
   `modul` varchar(45) DEFAULT NULL,
   `parameter` varchar(45) DEFAULT NULL,
   `operator` varchar(10) DEFAULT NULL,
   `vrednost` varchar(10) DEFAULT NULL,
   PRIMARY KEY (`programator`,`vrstica`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$db->q("
 CREATE TABLE `preveri` (
   `naslov` varchar(45) NOT NULL,
   PRIMARY KEY (`naslov`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

?>
To skripto boš morda moral pognati večkrat.

Ne pozabi dodati v /etc/rc.local
su -c "/home/lares/www/control.sh" lares &
