<?php

class dblink {

 public $link;
 private $creds;

 function __construct($database = "lares", $user = "lares", $pass = "password", $host = "localhost") {

  $this->creds = array(
   "db" => $database,
   "user" => $user,
   "pass" => $pass,
   "host" => $host
  );

  $this->link = new mysqli($host, $user, $pass, $database);
  $this->link->set_charset("utf8");

 }
 
 function q($q, $primary = "") {
    
  $r = $this->link->query($q);
  echo $this->link->error;

  $results = array();
  if($r !== True && $r !== False) {
   while($tmp = $r->fetch_assoc()) {
    if($primary == "") {
     $results[] = $tmp;
    } else {
     $results[$tmp[$primary]] = $tmp;
    }
   }
   return $results;
  } else {
   return $this->link->affected_rows;
  }

 }
 
 function e($e) {
  return $this->link->real_escape_string($e);
 }

 function flatten($array = array(), $parameter = "") {
  if($parameter == "") {
   reset($array);
   $tmp = $array[key($array)];
   reset($tmp);
   $parameter = key($tmp);
  }

  $out = array();
  foreach($array as $vnos) {
   $out[] = $vnos[$parameter]; 
  }

  return $out;
 }

 function export($mapa = "", $file = "") {
  if(trim($mapa) == "") {
   $file = "schemas";
  }

  if(trim($file) == "") {
   $file = "default";
  }
  
  $this->setMaintenence();

  exec('mysqldump --no-create-info --no-create-db --ignore-table='.escapeshellarg($this->creds["db"].'.db').' '.
       ' --user='.escapeshellarg($this->creds["user"]).
       ' --password='.escapeshellarg($this->creds["pass"]).
       ' --host='.escapeshellarg($this->creds["host"]).
       ' '.escapeshellarg($this->creds["db"]).
       ' > '.escapeshellarg(dirname(__FILE__).'/'.$mapa.'/'.$file.'.sql'));

  $this->clearMaintenence();

 }

 function import($mapa = "", $file = "") {
  if(trim($mapa) == "") {
   $file = "schemas";
  }

  if(trim($file) == "") {
   $file = "default";
  }

  $this->setMaintenence();

  exec('mysql '.
       ' --user='.escapeshellarg($this->creds["user"]).
       ' --password='.escapeshellarg($this->creds["pass"]).
       ' --host='.escapeshellarg($this->creds["host"]).
       ' '.escapeshellarg($this->creds["db"]).
       ' < '.escapeshellarg(dirname(__FILE__).'/'.$mapa.'/'.$file.'.sql'));

  $this->clearMaintenence();

 }

 function clear() {
  $this->setMaintenence();

  $tmp = $this->q("SHOW TABLES");
  $maintdb = False;
  foreach($tmp as $vnos) {
   if($vnos["Tables_in_".$this->creds["db"]] != "db") {
    $this->q("
     TRUNCATE `".$this->e($vnos["Tables_in_".$this->creds["db"]])."`
    ");
   } else {
    $maintdb = True;
   }
  }
  if(!$maintdb) {
   $this->createMaintenence();
  }

  $this->clearMaintenence();
 }

 function createMaintenence() {
  $this->q("
   CREATE TABLE `db` (
    `property` varchar(45) NOT NULL,
    `value` varchar(15) DEFAULT NULL,
     PRIMARY KEY (`property`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  ");
 }

 function checkMaintenence() {
  $tmp = $this->q("
   SELECT `value` FROM `db`
    WHERE `property` = 'maintenence'
  ");

  return ($tmp[0]["value"] == 1);
 }

 function setMaintenence($value = 1) {
  $this->q("
   INSERT INTO `db` (`property`, `value`) 
    VALUES ('maintenence', '".$this->e($value)."')
    ON DUPLICATE KEY UPDATE
     `value` = '".$this->e($value)."' 
  ");
 }

 function clearMaintenence() {
  $this->setMaintenence(0);
 }

 function __destruct() {
  $this->link->close();
 }

}

?>
