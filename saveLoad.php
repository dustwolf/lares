<?php

/*

 TODO
  * http://stackoverflow.com/questions/81934/easy-way-to-export-a-sql-table-without-access-to-the-server-or-phpmyadmin
  * `okvircki` in `programi`
  * podmapa kamor se shranjujejo file-i
  * GUI
   ** Seznam shranjenih
   ** Polje za ime + Save tipka

*/

if(isset($_POST) && !empty($_POST)) {

 require_once "mysqli.php";
 $db = new dblink();

 if($_POST["action"] == "save") {

  $db->export("schemas", $_POST["name"]);

 } elseif($_POST["action"] == "load") {

  $db->clear();
  $db->import("schemas", $_POST["name"]);

  header("Location: addRemove.php");

 } elseif($_POST["action"] == "reset") {

  $db->clear();

  header("Location: addRemove.php");

 }

}

require_once "html.php";
$doc = new html("LARES - Save or load control schemes", array(
 "bootstrap" => True, 
 "css" => "style.css"
));

include "menu.php";

?><div class="body-padding">

<h1>Save</h1>
<form method="POST">
 <input type="hidden" name="action" value="save">
 <label>scheme name</label>
 <input class="form-control" type="text" 
        name="name" value=""><br>
 <input class="form-control btn btn-success" type="submit" value="Save">
</form>

<h1>Load</h1>
<form method="POST">
 <input type="hidden" name="action" value="load">
 <label>select a scheme</label>
 <select class="form-control modul-picker" name="name" id="name">
  <?php
   $tmp = glob(dirname(__FILE__).'/schemas/*.sql');
   foreach($tmp as $vnos) { 
    $file = substr(basename($vnos),0,-4);
   ?>
    <option value="<?php echo $file; ?>"><?php echo $file; ?></option>
   <?php 
   }
  ?>
 </select><br>
 <input class="form-control btn btn-warning" type="submit" value="Load">
</form>

<h1>Reset</h1>
<form method="POST">
 <input type="hidden" name="action" value="reset">
 <label>This will reset the control scheme, so that you can start over.</label>
 <input class="form-control btn btn-danger" type="submit" value="Reset">
</form>

</div>
