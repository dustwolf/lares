<?php

require_once "../mysqli.php";
$db = new dblink();

$db->q("
 INSERT INTO `lares`.`okvircki`
 (`naslov`, `motor`, `stanje`)
 VALUES 
  ('".$db->e("Motor 1")."', '".$db->e("motor.php")."', '".$db->e(json_encode(array("vklop" => "1")))."'),
  ('".$db->e("Eksperiment")."', '".$db->e("eksperiment.php")."', '".$db->e(json_encode(array("vklop" => "1", "ime" => "E. Coli 1")))."'),
  ('".$db->e("Temperatura")."', '".$db->e("senzor.php")."', '".$db->e(json_encode(array("vklop" => "1", "port" => "1", "vrednost" => "22,5", "enota" => "°C")))."'),
  ('".$db->e("pH")."', '".$db->e("senzor.php")."', '".$db->e(json_encode(array("vklop" => "1", "port" => "2", "vrednost" => "7", "enota" => "")))."'),
  ('".$db->e("CO₂")."', '".$db->e("senzor.php")."', '".$db->e(json_encode(array("vklop" => "1", "port" => "3", "vrednost" => "25", "enota" => "%")))."'),
  ('".$db->e("Motor 2")."', '".$db->e("motor.php")."', '".$db->e(json_encode(array("vklop" => "0")))."'),
  ('".$db->e("Gretje / Hlajenje")."', '".$db->e("temperaturnaRegulacija.php")."', '".$db->e(json_encode(array("vklop" => "1", "nastavitev" => "22,5")))."'),
  ('".$db->e("Ventil 1")."', '".$db->e("ventil.php")."', '".$db->e(json_encode(array("vklop" => "1", "port" => "1")))."'),
  ('".$db->e("Ventil 2")."', '".$db->e("ventil.php")."', '".$db->e(json_encode(array("vklop" => "0", "port" => "2")))."')
");

?>
