<?php

class html {

 public $jQuery;
 public $chosen;
 public $bootstrap;
 public $rotate;
 public $css;

 private $jscache;

 function __construct($naslov = "", $opcije = array()) {
  $this->jQuery = isset($opcije["jQuery"]);
  $this->chosen = isset($opcije["chosen"]);
  $this->bootstrap = isset($opcije["bootstrap"]);
  $this->rotate = isset($opcije["rotate"]);
  if(isset($opcije["css"])) {
   $this->css = $opcije["css"];
  } else {
   $this->css = "";
  }
  if($this->chosen || $this->bootstrap || $this->rotate) {
   $this->jQuery = True;
  }

  $this->jscache = array();
 
?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <title><?php echo $naslov; ?></title>
  <?php if($this->jQuery) { ?>
   <script src="jquery-1.11.2.min.js"></script>
  <?php } ?>
  <?php if($this->bootstrap) { ?>
   <link rel="stylesheet" href="bootstrap.min.css">
   <link rel="stylesheet" href="bootstrap-theme.min.css">
  <?php } ?>
  <?php if($this->chosen) { ?>
   <link rel="stylesheet" href="chosen.css">
  <?php } ?>
  <?php if($this->rotate) { ?>
   <script src="jQueryRotateCompressed.js"></script>
  <?php } ?>
  <?php if($this->css != "") { ?>
   <link rel="stylesheet" href="<?php echo $this->css; ?>">
  <?php } ?>
 </head>
 <body>
<?php
 }

 function test() {
  ?>WORKS!<?php
 }

 function addJS ($id = "default", $js = "") {
  $this->jscache[$id] = "<script>".$js."</script>";
 }

 function hereJS() {
  echo implode("", $this->jscache);
  $this->jscache = array();
 }

 function __destruct() {
?>
 <?php if($this->bootstrap) { ?>
  <script src="bootstrap.min.js"></script>
 <?php } ?>
 <?php if($this->chosen) { ?>
  <script src="chosen.jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
   var config = {
    '.chosen-select' : {},
    '.chosen-select-deselect' : {allow_single_deselect:true},
    '.chosen-select-no-single' : {disable_search_threshold:10},
    '.chosen-select-no-results': {no_results_text:'Ni rezultatov.'},
    '.chosen-select-width' : {width:"95%"}
   }
   for (var selector in config) {
    $(selector).chosen(config[selector]);
   }
  </script>
 <?php } ?> 
 <?php 
  if($this->jscache != array()) {
   $this->hereJS(); 
  }
 ?>
 </body>
</html>
<?php
 }

}

class dropdown {

 private $izbrana;
 private $ena;
 
 function __construct($doc, $naslov = "", $nastavitev = "vzorec", $stanje = "-", $vezana = "", $kriterij = "runtime") {
  if(is_object($stanje)) {
   $this->izbrana = $stanje->{$nastavitev};
   if($this->izbrana === "") {
    $this->izbrana = "-";
   }
  } else {
   $this->izbrana = "-";
  }
  $this->ena = False;

  if($vezana == "") {
   $vezana = $nastavitev;
  } else {

   $doc->addJS($vezana,'
    jQuery("#'.$vezana.'").on("click", function() {
     window.tmp = jQuery("#parameter-'.$vezana.'").val();
     jQuery.ajax("ajaxEngines.php?modul="+encodeURIComponent(this.value)+"&kriterij="+encodeURIComponent("'.$kriterij.'")).done(
      function(data) { jQuery("#parameter-'.$vezana.'").html(data); jQuery("#parameter-'.$vezana.'").val(window.tmp); }
     );    
    });
   ');

   $vezana = "parameter-".$vezana;

  }
  ?>
  <label><?php echo $naslov; ?></label>
  <select class="form-control" name="<?php echo $nastavitev; ?>" id="<?php echo $vezana; ?>">
  <?php
 }

 function opcija($vrednost = "-") {
  $this->ena = True;
  ?>
  <option value="<?php echo $vrednost; ?>" <?php if($this->izbrana === $vrednost) { echo "selected"; } ?>>
   <?php echo $vrednost; ?>
  </option>  
  <?php
 }

 function __destruct() {
  if(!$this->ena) {
   $this->opcija();
  }
  ?>
  </select>
  <br>
  <?php
 }

} 

?>
