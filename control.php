#!/usr/bin/php
<?php

require_once "mysqli.php";
$db = new dblink();

if(!$db->checkMaintenence()) {

 require_once "engines.php";

 //POŽENI CONTROL VSEH OKVIRČKOV
 $okvircki = $db->q("
  SELECT `naslov`
   FROM `okvircki`
   ORDER BY `motor`
 ");

 foreach($okvircki as $okvircek) {

  $motor = new motor($okvircek["naslov"]);
  $motor->control();

 }

 //DODATNO POŽENI KONTROL VSEH DOUBLECHECK OKVIRČKOV
 $count=1;

 while($count > 0) {

  $okvircki = $db->q("
   SELECT `naslov`
    FROM `preveri`
  ");

  $count = count($okvircki);

  foreach($okvircki as $okvircek) {

   $motor = new motor($okvircek["naslov"]);
   $motor->control();

  }

  //SPRAZNI TABELO
  $db->q("
   TRUNCATE `preveri`
  ");

 }

}

/*

 TODO:
  * To bi morda moralo biti nekako rekurzivno...

*/

?>
