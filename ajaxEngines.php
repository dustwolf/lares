<?php

require_once "engines.php";

if(isset($_GET["modul"])) {
 
 if($_GET["modul"] !== 0) {

  $motor = new motor($_GET["modul"]);
  
  if(isset($motor->common->{$_GET["kriterij"]})) {
   $opcije = $motor->common->{$_GET["kriterij"]};
  } else {
   $opcije = array("-");
  }

  foreach($opcije as $opcija) {
   ?><option value="<?php echo $opcija; ?>"><?php echo $opcija; ?></option><?php
  }

 } else {
  ?><option value="0">-</option><?php
 }

}

?>
