<?php

/*

 TODO  
  * shemo za backend (bash / fasm), ki bo reguliral hardware in vmesnik z frontendom
  * senzor je delal tako:
   * FASM listener z FIFOtom
   * PHP uporablja exec >> da piše v FIFO
   * PHP periodično nalaga datoteke ki vsebujejo izhodne podatke
  * predlagam:
   * FASM listenerji z FIFOti
   * PHP uporablja exec >> da piše v FIFO

   * PHP ki registrira poslušalca tako da naredi pod /tmp/lares/host/app datoteko
   * FASM appenda podatke v vse /tmp/lares/host/app/data
   * PHP ki pobere vsebino /tmp/lares/host/app/data in zbriše file na request
   * AJAX ki periodično naloži datoteko in doda podatke na izpis

   * http://www.chartjs.org/docs/

 TODO:
  * Simulacija
  * Pralni stroj
  * Ajax ali periodični refresh, ki poskrbi da so podatki v GUI-u aktualni

*/

require_once "html.php";
$doc = new html("LARES - Control panel", array(
 "bootstrap" => True, 
 "rotate" => True, 
 "css" => "style.css"
));

require_once "mysqli.php";
$db = new dblink();

include "menu.php";

?><div class="body-padding"><?php

require_once "engines.php";

//ČE JE POST, SHRANI PODATKE
if(isset($_POST["id"])) {

 $izbran = $_POST["id"];
 
 $okvircki = $db->q("
  SELECT `naslov`, `motor`, `stanje` 
   FROM `okvircki`
   WHERE `naslov` = '".$db->e($izbran)."'
   LIMIT 1
 ");
 
 $okvircek = $okvircki[0];
 $motor = new motor($izbran, $okvircek["motor"], json_decode($okvircek["stanje"]), true, $doc);
 
 $motor->nastavi($_POST);

}

if(isset($_GET["fokus"])) {

 //NARISI IZBRANEGA KOT PRVEGA IN Z FOKUSOM
 $izbran = $_GET["fokus"];

 $okvircki = $db->q("
  SELECT `naslov`, `motor`, `stanje` 
   FROM `okvircki`
   WHERE `naslov` = '".$db->e($izbran)."'
   LIMIT 1
 ");

 $okvircek = $okvircki[0];
 $motor = new motor($okvircek["naslov"], $okvircek["motor"], json_decode($okvircek["stanje"]), true, $doc);
 $motor->gui();
 
} else {

 $izbran = "";

}

//NARISI OSTALE OKVIRCKE BREZ FOKUSA
$okvircki = $db->q("
 SELECT `naslov`, `motor`, `stanje` 
  FROM `okvircki`
  WHERE `naslov` NOT IN ('".$db->e($izbran)."')
  ORDER BY `motor`
");

foreach($okvircki as $okvircek) {

 $motor = new motor($okvircek["naslov"], $okvircek["motor"], json_decode($okvircek["stanje"]), false, $doc);
 $motor->gui();

}

?>

</div>
