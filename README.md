# LARES

LARES is a system designed for process control. This is it's software component.

Originally designed to be a commercial product, it's been open-sourced in the
hopes that someone on the Internet will find it useful.

## Screenshot
![Screenshot](screenshot.png)

## Design of the system

In hardware, LARES was designed to be a series of hardware controlled modules,
connected to an Ethernet bus. The software component was to run on an x86-based
control-box. And the system was to contain an off the shelf wireless router and 
off the shelf tablets with browsers for the user interface.

The software component was thus designed with graphical modules to reflect the 
hardware modules connected to the system, as well as additional virtual modules
for a user to be able to write automation without necessarily being a developer.

The user interface uses engineering symbols, animations and colors to indicate 
which hardware components are in operation. Icons and segmented circles are used 
to indicate the type of component, with physical components having fewer 
segments and more abstract ones having more.

## Architecture

The software component is composed of two separate parts. One is the user 
interface, that takes state out of the database and allows the user to 
manipulate it, as well as view the data.

The second part is the hardware control which interacts with the backend 
hardware modules, writes data logs, runs automation scripts and so forth.

In the source code, the invdividual components are isolated in separate folders
and use specifically named files to indicate which part of the application the 
file is relating to. An object-oriented approach is used to combine code shared
by multiple components.

## State of the source code

At present most of the source code is in Slovenian and is basically left off in 
a mid-development stage from 2015, at which point it was a closed-source 
project. I will be adding copyright notices and translating the project to 
english in the future.
